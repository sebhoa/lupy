# LuPy

Base de ressources ludiques pour programmer en Python.

## De quoi s'agit-il ?

D'un site _mkdocs_ pour recenser les idées de programmes ludiques à utiliser avec nos étudiant-es (le terme inclus les élèves de 1re et Tle).

**Qu'est-ce que c'est qu'une _idée de programme ludique_ ?**

- C'est un exercice de modélisation/programmation en langage Python (d'où le nom, Lu = ludique Py = Python)
- Ca peut être de n'importe quelle taille : un simple exercice de quelques dizaines de lignes de codes au projet complet de plusieurs centaines de lignes
- Sur la base d'une idée originale ou inspirée qui soit ludique : un jeu, un puzzle, une énigme, un _truc étonnant_...
- Cela peut être mathématique... ou pas (on essaiera de faire en sorte que ce ne soit pas **que** des exos de maths)

Bref vous l'aurez compris c'est relativement ouvert.


## Participer

Pour commencer à participer, il suffit de cloner ce _repo_ et de proposer quelque chose dans une des rubriques déjà en place puis faire une _merge request_. Et si l'utilisation du _git_ vous rebute, vous pouvez m'envoyer un mail avec votre idée, les fichiers dans un dossier et le nom de la rubrique dans laquelle votre idée doit figurer.


## Licence

Tout ce qui déposé ici est sous Licence Creative Commons 

![logo by-nc-sa](docs/assets/images/by-nc-sa.svg)