## Un assistant

Actuellement circule sur twitter un jeu qui est un mix entre le jeu du pendu et le mastermind : sutom.nocle.fr. Il s'agit de deviner un mot mystère, on propose un mot, la machine répond par un code de symboles pour dire si les lettres sont bien placées, mal placées ou absentes.

Exemple :

Le mot a découvrir est : B _ _ _ _ _ _

1. On essaie BICOQUE... la réponse est : B bien placé (normal il était déjà donné), puis 5 lettres qui ne figurent pas du tout dans le mot et enfin le E mal placé

2. On essaie BATELAS... réponse : A bien placée, T inexistant, E mal placé toujours, L inexistant etc.

Voici la trace complète de la partie du jour :

![sutom](sutom7.jpg){.centrer}

Le programme à proposer aux élèves : réaliser un assistant pour aider à trouver le mot mystère. Voici une trace de la partie avec l'assistant (le mien est en POO mais ce n'est pas une obligation).

## Trace d'une exécution

### Initialisation de l'assistant avec le motif du jour

```python
jeu = Sutom('b******')
print(jeu)
```
2141 possibilités...

### Essai 1

```python
jeu.saisie = 'bicoque'
jeu.reponse = '◼◻◻◻◻◻●'
jeu.update()
print(jeu)
```
235 possibilités...

### Essai 2

```python
jeu.saisie = 'batelas'
jeu.reponse = '◼◼◻●◻●◼'
jeu.update()
print(jeu)
```
23 possibilités...

### Essai 3

```python
jeu.saisie = 'bananes'
jeu.reponse = '◼◼◻◼◻◼◼'
jeu.update()
print(jeu)
```
basames
bavames
bayames
bagages

### Essai 4

```python
jeu.saisie = 'bagages'
jeu.reponse = '◼◼◼◼◼◼◼'
jeu.update()
print(jeu)
```
Gagné, le mot mystère était : BAGAGES

Je joins le code de ma classe Sutom pour celles et ceux que ça intéresse.
[sutom.py](sutom.py) et le [fichier texte des mots de la langue française](mots_avec_et_sans_accents.dic), avec et sans accents (il faudrait nettoyer ce fichier car les accents ne sont pas utiles)