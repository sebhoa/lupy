import random

class Sutom:
    
    # DIC_FILE = 'mots_avec_et_sans_accents.dic'
    DIC_FILE = 'mots.dic'
    ENCODING = 'utf-8'
    SEUIL = 20 # si le nb de mots admissibles est < seuil on affiche tous les mots
    JOKER = '*'
    ALPHABET = 'abcdefghijklmnopqrstuvwxyz'
    
    # codes réponses
    #
    # BIEN_PLACEE = '◼'
    # MAL_PLACEE = '●'
    # ABSENTE = '◻'
    
    BIEN_PLACEE = '1'
    MAL_PLACEE = '2'
    ABSENTE = '0'
    VALEURS =  {"a":1, "b":3, "c":3, "d":2, "e":1, "f":4, "g":2, "h":4, 
                "i":1, "j":8, "k":10, "l":1, "m":2, "n":1, "o":1, "p":3, 
                "q":8, "r":1, "s":1, "t":1, "u":1, "v":4, "w":10, "x":10, 
                "y":10, "z":10}
    
    @classmethod
    def clean_dic(cls, a_retirer):
        with open(Sutom.DIC_FILE) as dic:
            s = set(mot.strip() for mot in dic)
        s -= set(a_retirer)
        with open(Sutom.DIC_FILE, 'w', encoding='utf-8') as dic:
            for mot in s:
                dic.write(mot+'\n')
        
    
    def __init__(self, code_motif):
        if len(code_motif) == 2:
            lettre, self.taille = code_motif[0].lower(), int(code_motif[1])
            self.motif = [lettre] + ['*'] * (self.taille - 1) # les mots du dictionnaire sont en minuscules
        else:
            self.taille = len(code_motif)
            self.motif = list(code_motif)
        self.mots = set()        # ensemble des mots admissibles 
        self.dic = set()         # ensemble des mots du dictionnaire
        self.apparition = {}     # comptabilise avec certitude le nombre d'apparition d'une lettre dans le mot mystere
        self.certitude = set()   # on connait le nombre d'apparition avec certitude
        self.pos_interdites = {} # dictionnaire des positions interdites par lettre
        self.coup = 0
        self.load()
                
    def __str__(self):
        nb_mots = len(self.mots)
        if self.end():
            return f"Gagné, le mot mystère était : {''.join(self.motif).upper()}"
        elif nb_mots < Sutom.SEUIL:
            return '\n'.join(self.mots)
        else:
            return f'{nb_mots} possibilités...'
        
    def load(self):
        """Charge le dictionnaire et initialise les mots admissibles"""
        with open(Sutom.DIC_FILE, 'r', encoding=Sutom.ENCODING) as datas:
            for line in datas:
                self.dic.add(line.strip())
        self.mots = {mot for mot in self.dic if self.match(mot) and not self.interdit(mot)}
    
    def end(self):
        return Sutom.JOKER not in self.motif
    
    def position_interdite(self, lettre, pos):
        """renvoie True ssi pos est une position identifée comme interdite pour lettre"""
        return lettre in self.pos_interdites and pos in self.pos_interdites[lettre]
    
    def apparait_trop(self, lettre, nb_fois):
        """renvoie True ssi nb_fois c'est trop d'apparition pour lettre au regard des infos connues"""
        return lettre in self.apparition and nb_fois > self.apparition[lettre] and lettre in self.certitude
    
    def lettre_manquante(self, decompte):
        """les clés du dictionnaire pos_interdites sont des lettres qui sont présentes 
        dans le mot mystere... si cette lettre n'apparait pas dans le decompte des lettres
        du mot saisie c'est que la lettre manque et le mot sera donc rejeté"""
        return any(decompte[lettre] < self.apparition[lettre] for lettre in self.apparition) 

    def interdit(self, mot):
        """Renvoie True ssi
            - une lettre du mot apparait trop souvent ou 
            - une lettre est présente à une position où elle a été signifiée mal placée ou enfin
            - une lettre mal placée n'est pas du tout présente dans le mot
        """
        dans_le_mot = {lettre:0 for lettre in Sutom.ALPHABET}
        for pos, lettre in enumerate(mot):
            dans_le_mot[lettre] += 1
            if self.apparait_trop(lettre, dans_le_mot[lettre]):
                return True
            if self.position_interdite(lettre, pos):
                return True
        # à ce stade il n'y pas de lettre en trop ni de positions interdites
        # si une lettre manque le mot sera interdit sinon c'est ok
        return self.lettre_manquante(dans_le_mot)

    def match(self, mot):
        """Renvoie True ssi le mot mot coïncide avec le motif courant"""
        return len(mot) == self.taille and all((self.motif[i] == Sutom.JOKER) or (self.motif[i] == mot[i]) for i in range(len(mot)))        
    
    def get_infos_from_user(self, saisie, reponse):
        """renvoie l'ensemble des lettres absentes, le décompte des lettres
        de la proposition et met à jour les positions interdites apprises"""
        absentes = set()
        dans_saisie = {} # pour compter les apparitions dans la proposition du joueur
        
        # étape 1
        for i in range(self.taille):
            lettre, rep = saisie[i], reponse[i]
            if rep == Sutom.BIEN_PLACEE:
                self.motif[i] = lettre
                dans_saisie[lettre] = dans_saisie.get(lettre, 0) + 1
            elif rep == Sutom.MAL_PLACEE:
                self.pos_interdites[lettre] = self.pos_interdites.get(lettre, set()) | {i}
                dans_saisie[lettre] = dans_saisie.get(lettre, 0) + 1
            elif rep == Sutom.ABSENTE:
                absentes.add(lettre)
        return absentes, dans_saisie
        
        
    def update(self, saisie, reponse):
        """Met à jour la liste des mots admissibles avec les contraintes courantes"""
        # étape 1
        absentes, dans_saisie = self.get_infos_from_user(saisie, reponse)
        
        # étape 2 : figer l'info des lettres déclarées absentes
        for lettre in absentes:
            self.certitude.add(lettre)
            self.apparition[lettre] = dans_saisie.get(lettre, 0)
        
        # étape 3 : si une lettre saisie n'a pas été déclarée absente, son apparition 
        # dans le mot mystère est au moins égale à son apparition dans la saisie
        for lettre in dans_saisie:
            if lettre not in self.certitude:
                self.apparition[lettre] = dans_saisie[lettre]
        
        # étape 4 : mettre à jour les most admissibles
        self.mots = {mot for mot in self.mots if self.match(mot) and not self.interdit(mot)}
    
    def suggestion(self):
        l_sugg = [mot for mot in self.mots if all(mot.count(lettre) == 1 for lettre in mot)]
        nb = min(len(l_sugg), Sutom.SEUIL)
        random.shuffle(l_sugg)
        for i in range(nb):
            print(l_sugg[i])
    
    def score(self, mot):
        sc = int(len(set(mot)) / len(mot) * 100) # if len(self.mots) > Sutom.SEUIL else 0
        return sc - sum(Sutom.VALEURS[lettre] for lettre in mot)
        
    def resoudre(self):
        choix = sorted(self.mots, key=self.score, reverse=True)
        while not self.end():
            i = 0
            reponse = ''
            print(f'Il y a {len(self.mots)} possibilités.')
            while reponse == '':
                mot = choix[i]
                print(f'Essayons : {mot}')
                reponse = input("La réponse de l'IA : ")
                i += 1
            self.coup += 1
            self.update(mot, reponse)
            choix = sorted(self.mots, key=self.score, reverse=True)
            #choix = list(self.mots)
            #random.shuffle(choix)
                
    def dialogue(self):
        saisie, reponse  = '', ''
        while len(saisie) != self.taille:
            saisie = input('Votre proposition  : ')
        while len(reponse) != self.taille:
            reponse = input("La réponse de l'IA : ")
        self.update(saisie, reponse)
        print(self)
    
    def ajout_dic(self, mot):
        self.dic.add(mot)
        with open(Sutom.DIC_FILE, 'a', encoding='utf-8') as dico:
            dico.write(mot+'\n')