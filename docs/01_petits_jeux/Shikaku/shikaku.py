from cmath import rect


class Rect:
    
    def __init__(self, rect_id, area, x, y, shikaku):
        self.id = rect_id
        self.shikaku = shikaku
        self.x = x
        self.y = y
        self.area = area
        self.initial_positions = set()
        self.positions = set()  # (x, y, w, h)
        self.filled = False
        
    def upper_left(self, width, height):
        xmin = max(self.x - width + 1, 0)
        xmax = min(self.x, self.shikaku.width - width)
        ymin = max(self.y - height + 1, 0)
        ymax = min(self.y, self.shikaku.height - height)
        return xmin, xmax, ymin, ymax
    
    def enough_space(self, x, y, w, h):
        return all(self.shikaku.empty(x+dx, y+dy) for dx in range(w) for dy in range(h) if not self.is_origine(x+dx, y+dy))

    def decompose(self):
        """looking for all decomposition width x height of the given area"""
        self.sizes = {(w, self.area // w) for w in range(1, self.area+1) 
                                                if self.area % w == 0}

    def init_positions(self):
        """Initialise toutes les possibilités de positionnement"""
        self.decompose()
        for w, h in self.sizes:
            xmin, xmax, ymin, ymax = self.upper_left(w, h)
            for x in range(xmin, xmax+1):
                for y in range(ymin, ymax+1):
                    if self.enough_space(x, y, w, h):
                        self.initial_positions.add((x, y, w, h)) 
    
    def update(self):
        self.positions = {position for position in self.initial_positions if self.enough_space(*position)}

    def nb_solutions(self):
        return len(self.positions)
        
    def origine(self):
        return self.x, self.y

    def is_origine(self, x, y):
        """is x, y the position of the first area-value in the shikaku problem?"""
        return self.x == x and self.y == y
    

class Shikaku:
    
    LABELS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'

    def __init__(self):
        self.width = 0
        self.height = 0
        self.grid = []
        self.rectangles = []
        self.solutions = []
    
    def __str__(self):
        return '\n'.join(''.join(str(e) if e != 0 else '.' for e in line) for line in self.grid)

    def empty(self, x, y):
        return self.grid[y][x] == 0

    def load_with_file(self, filename):
        rect_id = 0
        with open(filename) as datas:
            self.width, self.height = [int(i) for i in datas.readline().split()]
            for y in range(self.height):
                new_line = []
                for x, v in enumerate(datas.readline().split()):
                    v = int(v)
                    new_line.append(v)
                    if v:
                        rect = Rect(rect_id, v, x, y, self)
                        rect_id += 1
                        self.rectangles.append(rect)
                self.grid.append(new_line)
        self.init_positions()

    def load(self):
        self.width, self.height = [int(i) for i in input().split()]
        rect_id = 0
        for y in range(self.height):
            new_line = []
            for x, v in enumerate(input().split()):
                v = int(v)
                new_line.append(v)
                if v:
                    rect = Rect(rect_id, v, x, y, self)
                    rect_id += 1
                    self.rectangles.append(rect)
            self.grid.append(new_line)
        self.init_positions()

    def init_positions(self):
        for rect in self.rectangles:
            rect.init_positions()
        
    def fill(self, x, y, w, h, value):
        for dx in range(w):
            for dy in range(h):
                self.grid[y+dy][x+dx] = value
    
    def unfill(self, x, y, w, h, rect):
        for dx in range(w):
            for dy in range(h):
                self.grid[y+dy][x+dx] = 0 if not rect.is_origine(x+dx, y+dy) else rect.area
        
    def update(self):
        for rect in self.rectangles:
            if not rect.filled:
                rect.update()
    
    def is_solution(self):
        return all(not self.empty(x, y) for x in range(self.width) for y in range(self.height)) 
    
    def intersect(self, origine, position):
        x, y, w, h = position
        for dx in range(w):
            for dy in range(h):
                dest = x+dx, y+dy
                if (dest != origine) and not self.empty(*dest):
                    return True
        return False

    def can_touch(self, xx, yy, rect):
        origine = rect.origine()
        for position in rect.positions:
            if self.inside(xx, yy, *position) and not self.intersect(origine, position):
                return True
        return False

    def inside(self, a, b, x, y, w, h):
        return x <= a < x+w and y <= b < y+h

    def isolated_point(self, x, y, rx, ry):
        candidates = [rect for rect in self.rectangles if not rect.is_origine(rx, ry) and not rect.filled and self.can_touch(x, y, rect)]
        return candidates == []    

    def small_holes(self, x, y, w, h, rx, ry):
        for xx in range(self.width):
            for yy in range(self.height):
                if self.empty(xx, yy) and not self.inside(xx, yy, x, y, w, h) and self.isolated_point(xx, yy, rx, ry):
                    return True
        return False

    def solve(self, solution, prof):
        if self.is_solution():
            self.solutions.append(solution.copy())
        else:
            self.update()
            rectangles = [rect for rect in sorted(self.rectangles, key=Rect.nb_solutions) if rect.nb_solutions() > 0 and not rect.filled]
            rect = rectangles[0]
            # positions = [pos for pos in rect.positions] if rectangles else []
            # for i, rect in enumerate(rectangles):
            # print(f'PROF {prof}, RECT {rect.area} en {rect.x},{rect.y}')
            for i, (x, y, w, h) in enumerate(rect.positions):
                # print(f'{x},{y} {w}x{h} -- {i+1}/{len(rect.positions)}' )
                # nouvelle config
                self.fill(x, y, w, h, Shikaku.LABELS[rect.id])
                solution.append((x, y, w, h))
                # self.update()
                rect.filled = True

                if not self.small_holes(x, y, w, h, rect.x, rect.y):
                    # print('PASSE')
                    # print(self)
                    # input()
                    # appel recursif
                    self.solve(solution, prof+1)
                    # print('<<< BACK')
                # else:
                #     print('REJETE')
                #     print(self)
                #     input()

                # retour config initiale
                self.unfill(x, y, w, h, rect)
                solution.pop()
                rect.filled = False
                self.update()


    def answer(self):
        best = self.format(self.solutions[0])
        for j in range(1, len(self.solutions)):
            sj = self.format(self.solutions[j])
            if sj < best:
                best = sj
        print(len(self.solutions))
        print(best)

    def format(self, solution):
        infos = sorted(solution, key=lambda e: (e[1], e[0]))
        for i, (x, y, w, h) in enumerate(infos):
            self.fill(x, y, w, h, Shikaku.LABELS[i])
        return str(self)

    def solution(self):
        self.load()
        self.solve([])
        self.answer()


# puzzle = Shikaku()
# puzzle.solution()