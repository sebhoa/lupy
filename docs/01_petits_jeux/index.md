hide:
    -toc

---

# :fontawesome-solid-dice: Petits jeux

Dans cette rubrique, vous trouverez des exemples de petits jeux prétextes à programmation. On pourra faire programmer le jeu, certes ; mais on pourra aussi réaliser des modélisations pour aider le joueur à comprendre le jeu ou à trouver une stratégie gagnante.
