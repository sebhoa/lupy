
import pyxel

FILE_GRID = 'pacman_grid.csv'

# --- DIMENSIONS DIVERSES

CELL_SIZE = 18

# les valeurs ci-dessous ne doivent pas changer : elles sont 
# exactement celles liées au fichier csv pacman_grid.csv
COLS = 28
ROWS = 31

# Les pixels
HEIGHT = ROWS * CELL_SIZE
WIDTH = COLS * CELL_SIZE

HEADER_HEIGHT = 30
MARGIN = 5

PACMAN_X = 13
PACMAN_Y = 17
TUNNEL_Y = 14

START_GHOST_X = 11, 16
START_GHOST_Y = 13, 15

WIDTH_TEXT = 25

# --- STR
# les caractères ci-dessous sont issus du canadien autochtone

TITLE = "Pᗣᗧ•••MᗣN"

# me sert pas de ceux là mais ils sont cools non ?
PM_UP = 'ᗢ'
PM_DOWN = 'ᗣ'
PM_LEFT = 'ᗤ'
PM_RIGHT = 'ᗧ'
GHOST = 'ᗝ'
GHOST_2 = 'ᙁ'


# --- IMAGES

PNG_PM_LEFT = 'assets/PM_LEFT.png'
PNG_PM_RIGHT = 'assets/PM_RIGHT.png'
PNG_PM_UP = 'assets/PM_UP.png'
PNG_PM_DOWN = 'assets/PM_DOWN.png'

# PYXRES

SPRITES_SIZE = 16

PM_RIGHT = (0, 0, 0)
PM_LEFT = (0, 16, 0)
PM_UP = (0, 32, 0)
PM_DOWN = (0, 48, 0)

PM_SPRITES = {(0, 0): PM_RIGHT, (1, 0): PM_RIGHT, (0, -1): PM_LEFT, (-1, 0): PM_UP, (0, 1): PM_DOWN}

BLACK_BLOCK = (0, 16, 32)
BLUE_BLOCK = (0, 0, 48)
FEED_BLOCK = (0, 48, 32)
VITAMIN_BLOCK = (0, 32, 32)

GREY_GHOST = (0, 0, 16)
BLUE_GHOST = (0, 16, 16)
RED_GHOST = (0, 32, 16)
GREEN_GHOST = (0, 48, 16)
FEARED_GHOST = (0, 0, 32)

GHOST_SPRITES = [GREY_GHOST, BLUE_GHOST, RED_GHOST, GREEN_GHOST, FEARED_GHOST]

DIGITS = [(0, 16, 48), (0, 32, 48), (0, 48, 48),
            (0, 0, 64), (0, 16, 64), (0, 32, 64), (0, 48, 64), 
            (0, 0, 80), (0, 16, 80), (0, 32, 80)]

GAME = (0, 0, 96), (0, 16, 96), (0, 32, 96)
OVER = (0, 0, 112), (0, 16, 112), (0, 32, 112)

# COLORS

BLACK = 0
WHITE = 7
RED = 8
YELLOW = 10
DARK_BROWN = 4
DARK_GREEN = 3
DARK_BLUE = 1
DARK_PINK = 2

BROWN = 9
LIGHT_BROWN = 15
LIGHT_GREEN = 11
BLUE = 5
LIGHT_BLUE = 6

PINK = 14


# CELLS TYPE, CELL COLORS, POINTS and LEVELS

WALL0 = 0
WALL1 = 1
FEED = 2
VITAMIN = 3
EMPTY = 4
CHERRY = 5
STRAWBERRY = 6
ORANGE = 7
APPLE = 8
MELON = 9

POINTS = {WALL0:0, WALL1:0, 
    FEED: 10, VITAMIN: 50, 
    EMPTY: 0,
    CHERRY: 100, 
    STRAWBERRY: 300, 
    ORANGE: 500,
    APPLE: 700,
    MELON: 1000
    }

GHOSTS_POINTS = 200

COLORS = {WALL0:DARK_BLUE, WALL1:BLACK, 
    FEED: BLACK, VITAMIN: BLACK, 
    EMPTY: BLACK, 
    STRAWBERRY: BLACK, 
    ORANGE: BLACK,
    APPLE: BLACK,
    MELON: BLACK
    }

GRID_SPRITES = {WALL0: BLUE_BLOCK,
                WALL1: BLACK_BLOCK,
                FEED: FEED_BLOCK,
                VITAMIN: VITAMIN_BLOCK,
                EMPTY: BLACK_BLOCK}
# STRAWBERRY = 5
# ORANGE = 6
# APPLE = 7
# MELON = 8FEED: (0, 16, 32), VITAMIN: (0, 32, 32)}

LEVELS = {2:STRAWBERRY, 3:ORANGE, 4:ORANGE, 5:APPLE, 6:APPLE, 7:MELON, 8:MELON}

# EVENTS

ARROW_KEYS = {pyxel.KEY_LEFT: (-1, 0), pyxel.KEY_RIGHT: (1, 0),
            pyxel.KEY_UP: (0, -1), pyxel.KEY_DOWN: (0, 1)}
DIRECTIONS = ARROW_KEYS.values()

FPS = 6

# SOUNDS

