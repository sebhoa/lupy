import random
import time
from collections import deque
from constantes import *

def xy_to_pixels(x, y):
    return x * CELL_SIZE, y * CELL_SIZE + HEADER_HEIGHT

def alea(a, b):
    return random.randint(a, b)

class Ghost:

    def __init__(self, x, y, pid, grid, vitesse=1):
        self.grid = grid
        self.x = x
        self.y = y
        self.old_position = x, y
        self.id = pid
        self.feared = False
        self.targets = []
        self.seen = set()
        self.vitesse = vitesse
        self.dead = False

    def position(self):
        return self.x, self.y

    def die(self):
        self.dead = True

    def fear(self):
        self.feared = True


    def wait(self):
        x0, x1 = START_GHOST_X
        y0, y1 = START_GHOST_Y
        cells = [(x, y) for x, y in self.grid.neighbors(self.x, self.y) if
                    x0 <= x <= x1 and y0 <= y <= y1]
        self.x, self.y = cells[random.randrange(len(cells))] 

    def bfs(self, target):
        paths = deque()
        paths.append(target)
        found = False
        while not found:
            x, y = paths.popleft()
            for new_cell in self.grid.neighbors(x, y):
                if new_cell == self.position():
                    return x, y
                elif new_cell not in paths:
                    paths.append(new_cell)

    def tracking(self, target):
        if not self.dead:
            if self.targets:
                self.x, self.y = self.targets.pop()
            else:
                neighbors = self.grid.neighbors(self.x, self.y)
                pacman = self.grid.pacman.position()
                if pacman in neighbors:
                    self.x, self.y = pacman
                else:
                    next_destination = self.bfs(target)
                    self.targets = [next_destination for _ in range(self.vitesse)]

    def draw(self):
        self.grid.erase(*xy_to_pixels(*self.old_position))
        if not self.dead:
            pid = -1 if self.feared else self.id
            self.grid.generic_draw_img(*xy_to_pixels(self.x, self.y), *GHOST_SPRITES[pid])


class Randy(Ghost):
    """Fantôme qui bouge aléatoirement"""

    def __init__(self, *args):
        Ghost.__init__(self, *args)

    def move(self):
        if not self.dead:
            if self.targets:
                self.x, self.y = self.targets.pop()
            else:
                neighbors = self.grid.neighbors(self.x, self.y)
                cells = [(x, y) for x, y in neighbors if (x, y) not in self.seen]
                self.old_position = self.x, self.y
                if cells == []:
                    cells = neighbors
                indice = random.randrange(len(cells))
                self.targets = [cells[indice] for _ in range(self.vitesse)]
                self.seen.add(cells[indice])

    # def move(self):
    #     if not self.dead:
    #         neighbors = self.grid.neighbors(self.x, self.y)
    #         cells = [(x, y) for x, y in neighbors if (x, y) not in self.seen]
    #         self.old_position = self.x, self.y
    #         if cells == []:
    #             cells = neighbors
    #         indice = random.randrange(len(cells))
    #         self.x, self.y = cells[indice]
    #         self.seen.add(cells[indice])


class Angry(Ghost):
    """Fantôme qui traque Pacman avec BFS"""

    def __init__(self, *args):
        Ghost.__init__(self, *args)

    def move(self):
        # changer pour faire aller le fantome qq part 
        # vers une zone en avant de pacman
        # la distance est supérieure à un seuil
        # sinon foncer sur pacman
        target = self.grid.pacman.front()
        self.tracking(target)

class Friendly(Ghost):
    """Fantôme qui suis Pacman en BFS"""

    def __init__(self, *args):
        Ghost.__init__(self, *args)

    def move(self):
        # changer pour faire aller le fantome qq part vers le dos si
        # la distance est supérieure à un seuil
        # sinon foncer sur pacman
        target = self.grid.pacman.back()
        self.tracking(target)



class PacMan:

    def __init__(self, x, y, grid):
        self.grid = grid
        self.x = x
        self.y = y
        self.old_position = x, y
        self.direction = 0, 0
        self.strong = False
        self.dead = False

    def position(self):
        return self.x, self.y

    def back(self):
        dx, dy = self.direction
        x = self.x - dx
        y = self.y - dy
        if self.grid.ok(x, y):
            return x, y
        else:
            return self.x, self.y
    
    def front(self):
        dx, dy = self.direction
        x = self.x + dx
        y = self.y + dy
        if self.grid.ok(x, y):
            return x, y
        else:
            return self.x, self.y

    def die(self):
        self.dead = True

    def move(self):
        dx, dy = self.direction
        nx, ny = self.x + dx, self.y + dy
        if ny == TUNNEL_Y:
            nx = nx % COLS
        if self.grid.ok(nx, ny):
            self.old_position = self.x, self.y
            self.x, self.y = nx, ny
    
    def change_direction(self, dx, dy):
        nx, ny = self.x + dx, self.y + dy
        if ny == TUNNEL_Y:
            nx = nx % COLS
        if self.grid.ok(nx, ny):
            self.direction = dx, dy

    def draw(self):
        self.grid.erase(*xy_to_pixels(*self.old_position))
        if not self.dead:
            self.grid.generic_draw_img(*xy_to_pixels(self.x, self.y), *PM_SPRITES[self.direction])


class Grid:
    """Le labyrinthe"""

    def __init__(self, game):
        self.game = game
        self.pacman = PacMan(PACMAN_X, PACMAN_Y, self)
        self.ghosts = [Randy(alea(*START_GHOST_X), alea(*START_GHOST_Y), 0, self), Randy(alea(*START_GHOST_X), alea(*START_GHOST_Y), 1, self),Angry(alea(*START_GHOST_X), alea(*START_GHOST_Y), 2, self), Friendly(alea(*START_GHOST_X), alea(*START_GHOST_Y), 3, self)]
        self.cells = {}

    def load(self):
        with open(FILE_GRID) as lines:
            for y, line in enumerate(lines):
                for x, s_val in enumerate(line.strip().split(',')):
                    self.cells[x, y] = int(s_val)
    
    def pacman_move(self, new_direction):
        self.pacman.change_direction(*new_direction)
    
    def ghosts_move(self):
        if self.game.spent() > 5:
            for ghost in self.ghosts:
                ghost.move()
        else:
            for ghost in self.ghosts:
                ghost.wait()


    def fear_ghosts(self):
        for ghost in self.ghosts:
            ghost.fear()

    def count_feared(self):
        return sum(ghost.feared for ghost in self.ghosts)

    def pacman_eat(self):
        pm_x, pm_y = self.pacman.position()
        content = self.cells[pm_x, pm_y]
        self.game.inc_score(POINTS[content])
        self.cells[pm_x, pm_y] = EMPTY
        if content == VITAMIN:
            self.fear_ghosts()

    def wall(self, x, y):
        val = self.cells[x, y]
        return val == WALL0 or val == WALL1

    def ok(self, x, y):
        return 0 <= x < COLS and 0 <= y < ROWS and not self.wall(x, y)

    def neighbors(self, x, y):
        cells = []
        for dx, dy in DIRECTIONS:
            nx, ny = x + dx, y + dy
            if ny == TUNNEL_Y:
                nx = nx % COLS
            if self.ok(nx, ny):
                cells.append((nx, ny))
        return cells

    def collisions(self):
        pm = self.pacman
        for ghost in self.ghosts:
            if ghost.position() == pm.position():
                if ghost.feared:
                    ghost.die()
                    self.game.inc_score(GHOSTS_POINTS * (self.count_feared() + 1))
                else:
                    self.pacman.die()
                    self.game.game_over()

    def animate(self):
        self.pacman.move()
        self.pacman_eat()
        self.collisions()
        if not self.game.end:
            for ghost in self.ghosts:
                ghost.move()
                self.collisions()


    # -- abour view

    def generic_draw_img(self, x, y, page_id, tile_x, tile_y, width=SPRITES_SIZE, height=SPRITES_SIZE):
        pyxel.blt(x, y, page_id, tile_x, tile_y, width, height)

    def xy(self):
        return ((x, y) for y in range(ROWS) for x in range(COLS))

    def draw_laby(self):
        """draw laby"""
        for x, y in self.xy():
            self.generic_draw_img(*xy_to_pixels(x, y), *GRID_SPRITES[self.cells[x, y]])

    def erase(self, x, y):
        """erase a cell ie paint in black"""
        self.generic_draw_img(x, y, *BLACK_BLOCK)


    def draw(self):
        """draw all moving entities"""
        self.draw_laby()
        self.pacman.draw()
        for ghost in self.ghosts:
            ghost.draw()



class Game:
    """Le contrôleur du jeu"""

    def __init__(self):
        pyxel.init(WIDTH, HEIGHT + HEADER_HEIGHT, title=TITLE, fps=FPS)
        self.grid = Grid(self)
        self.score = 0
        self.end = False
        self.debut = int(time.time())

    def time(self):
        return int(time.time())
    
    def spent(self):
        return self.time() - self.debut

    def start(self):
        pyxel.load('pacman.pyxres')
        self.grid.load()
        self.grid.draw()

    def event_handle(self):
        for key in ARROW_KEYS:
            if pyxel.btn(key):
                self.grid.pacman_move(ARROW_KEYS[key])

    def inc_score(self, pts):
        self.score += pts

    def game_over(self):
        self.end = True
        self.aff_game_over()

    def update(self):
        if not self.end:
            self.event_handle()
            self.grid.animate()

    def aff_game_over(self):
        x, y = WIDTH // 2 - WIDTH_TEXT, HEIGHT // 2
        for i in range(3):
            self.grid.generic_draw_img(x, y, *GAME[i])
            x += SPRITES_SIZE        
        x, y = WIDTH // 2 - WIDTH_TEXT, HEIGHT // 2 + SPRITES_SIZE
        for i in range(3):
            self.grid.generic_draw_img(x, y, *OVER[i])
            x += SPRITES_SIZE            

    def aff_score(self):
        x, y = WIDTH // 2 - 45, MARGIN
        s_score = f'{self.score:07}'
        for _, d in enumerate(s_score):
            self.grid.erase(x, y)
            self.grid.generic_draw_img(x, y, *DIGITS[int(d)])
            x += 12

    def draw(self):
        if not self.end:
            self.aff_score()
            self.grid.draw()
    
    def run(self):
        pyxel.run(self.update, self.draw)

test = Game()
test.start()
test.run()
