"""
Shikaku puzzle modélisation POO
backtracking inspiré de la solution de F.Junier

Author: S. Hoarau
"""

class VirtualRect:
    """Modélise un rectangle virtuel du puzzle shikaku
    soit une aire et les coordonnées d'une case qu'on appelle origine. 
    De plus, chaque VirtualRect possède un shikaku auquel il appartient
    ainsi qu'un ensemble de rectangles réels qui contiennent l'origine et dont l'aire est identique"""
    
    def __init__(self, area, x, y, shikaku):
        # --- Le modèle du rectangle virtuel
        self.shikaku = shikaku  # le shikaku d'origine
        self.x = x              # les coordonnées
        self.y = y              # de la case obligatoire (origine)
        self.area = area        # la valeur de l'aire
        
        # --- Pour la résolution
        self.rectangles = {} # (x, y) -> {(w, h) possibles }
        self.filled = False     # True ssi le rectangle a été positionné
    
    def upper_left(self, width, height):
        """Calcule l'intervalle de valeurs possibles pour x et pour y
        les coordonnées du coin supérieur gauche d'un emplacement possible
        du rectangle... width et height sont les dimensions du shikaku"""
        xmin = max(self.x - width + 1, 0)
        xmax = min(self.x, self.shikaku.width - width)
        ymin = max(self.y - height + 1, 0)
        ymax = min(self.y, self.shikaku.height - height)
        return xmin, xmax, ymin, ymax
    
    def enough_space(self, x, y, w, h):
        """Renvoie True ssi le positionnement en (x, y) sur w de large et h de haut entre dans l'espace restant"""
        return self.shikaku.enough_space(x, y, w, h, self.x, self.y)

    def decompose(self):
        """Calcule toutes les décompositions (w, h) tel w * h = aire du rectangle"""
        return {(w, self.area // w) for w in range(1, self.area+1) 
                                                if self.area % w == 0}
    def init_rectangles(self):
        """Initialise toutes les possibilités de positionnement"""
        for w, h in self.decompose():
            xmin, xmax, ymin, ymax = self.upper_left(w, h)
            for x in range(xmin, xmax+1):
                for y in range(ymin, ymax+1):
                    if self.enough_space(x, y, w, h):
                        self.rectangles[x, y] = self.rectangles.get((x, y), set()) | {(w, h)}
            
    def origine(self):
        return self.x, self.y
        

class Shikaku:
    """Modélise le puzzle : une dimension, une grid vide qu'on av d'abord remplir avec des entiers via un fichier de données, des rectangles, et une liste de solutions"""
    
    LABELS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'

    def __init__(self):
        self.width = 0
        self.height = 0
        self.grid = []
        self.virtuals = []
        self.solutions = [] # la liste des chaines de caractères solutions
    
    def __str__(self):
        return '\n'.join(''.join(str(e) if e != 0 else '.' for e in line) for line in self.grid)

    def empty(self, x, y):
        return self.grid[y][x] == 0

    def is_origine(self, x, y):
        """Renvoie True si la cse de coordonnées x, y est une origine
        ie que sa valeur dans la grille est un entier > 0"""
        v = self.grid[y][x]
        return isinstance(v, int) and v > 0

    def enough_space(self, xr, yr, w, h, xo, yo):
        """Renvoie True si le rectangle dont le coin supérieur gauche est en xr, yr, de largeur w, hauteur h et dont le VirtualRect a pour origine xo, yo ne croise pas un autre rectangle"""
        return all(self.empty(xr+dx, yr+dy) for dx in range(w) for dy in range(h) if (xr+dx, yr+dy) != (xo, yo))

    def load_with_file(self, filename):
        """la version avec un fichier, par exemple si on travaille dans un notebook"""
        with open(filename) as datas:
            self.width, self.height = [int(i) for i in datas.readline().split()]
            for y in range(self.height):
                new_line = []
                for x, value in enumerate(datas.readline().split()):
                    value = int(value)
                    new_line.append(value)
                    if value:
                        vr = VirtualRect(value, x, y, self)
                        self.virtuals.append(vr)
                self.grid.append(new_line)
        self.init_positions()

    def load(self):
        """La version pour codingame"""
        self.width, self.height = [int(i) for i in input().split()]
        for y in range(self.height):
            new_line = []
            for x, value in enumerate(input().split()):
                value = int(value)
                new_line.append(value)
                if value:
                    vr = VirtualRect(value, x, y, self)
                    self.virtuals.append(vr)
            self.grid.append(new_line)
        self.init_positions()

    def empty_coords(self):
        return ((x, y) for x in range(self.width) for y in range(self.height) if self.empty(x, y))

    def unfilled_areas(self):
        return (vr for vr in self.virtuals if not vr.filled)

    def init_positions(self):
        for vr in self.virtuals:
            vr.init_rectangles()
        
    def fill(self, x, y, w, h, value):
        for dx in range(w):
            for dy in range(h):
                self.grid[y+dy][x+dx] = value
    
    def unfill(self, x, y, w, h, virtual):
        for dx in range(w):
            for dy in range(h):
                self.grid[y+dy][x+dx] = 0 if (x+dx, y+dy) != virtual.origine() else virtual.area

    def next_cell(self, x, y, dx=1):
        x += dx
        if x >= self.width:
            x = 0
            y += 1
        return x, y

    def solution_reached(self, y):
        """Renvoie True ssi la coordonnée y du la cellule courante à remplir et supérieure à la hauteur du shikaku"""
        return y >= self.height 
    
    def inside(self, a, b, x, y, w, h):
        """Le point (a, b) est-il à l'intérieur du rectangle (x,y) wxh ?"""
        return x <= a < x+w and y <= b < y+h
    
    def solve(self, x, y, letter_id):
        """Parcours l'arbre d'exploration pour récupérer toutes les solutions"""
        if self.solution_reached(y):
            self.solutions.append(str(self))
        elif not self.empty(x, y) and not self.is_origine(x, y):
            x, y = self.next_cell(x, y)
            self.solve(x, y, letter_id)
        else:
            for vr in self.unfilled_areas():
                if (x, y) in vr.rectangles:
                    xo, yo = vr.origine()
                    for w, h in vr.rectangles[x, y]:
                        if self.empty_rect(x, y, w, h, xo, yo):
                            self.fill(x, y, w, h, Shikaku.LABELS[letter_id])
                            vr.filled = True
                            nx, ny = self.next_cell(x, y, w)
                            self.solve(nx, ny, letter_id+1)
                            self.unfill(x, y, w, h, vr)
                            vr.filled = False

    def answer(self):
        print(len(self.solutions))
        self.solutions.sort()
        print(self.solutions[0])

    def solution(self):
        self.load()
        self.solve(0, 0, 0)
        self.answer()

puzzle = Shikaku()
puzzle.solution()