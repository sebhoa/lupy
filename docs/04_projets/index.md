hide:
    - toc

---


# :fontawesome-solid-scroll: Les projets

Dans cette rubrique, vous trouverez des projets de niveau variable : de 1re NSI à 1re année universitaire. Certains ont été testé sur des élèves par des collègues d'autres sont des idées de projets non encore expérimentés.

Il peut s'agir de mettre à disposition des énoncés déjà bien rédigés, pratiquement prêts à l'emploi dans une classe, comme de laisser un _brouillon_ plus ou moins abouti d'une idée.  

Si vous vous emparez d'une de ces idées et que vous en faites une ressource aboutie pour vos élèves, merci de la redistribuer ensuite par ce silte collaboratif.