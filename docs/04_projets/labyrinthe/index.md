# Labyrinthe

Voici un mini projet sur la génération, la visualisation et la résolution de labyrinthes. La visualisation utilisera ipythonblocks et donc tout se passe dans un notebook divisé en 3 parties :

- découverte et manipulation des _blockgrid_ du module ipythonblocks
- modélisation et génération d'un labyrinthe
- résolution du labyrinthe

[Telécharger le notebook de l'activité](td_laby.ipynb)