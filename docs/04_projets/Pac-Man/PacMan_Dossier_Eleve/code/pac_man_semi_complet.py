from math import inf
from tkinter import *
from random import choice

class Lab:
    
    def __init__(self, l: list):
        self.grille = l
        self.graphe = {}
        for i in range(len(l)):
            for j in range(len(l[0])):
                if l[i][j] != 1:
                    self.graphe[(i, j)] = []
                    if i > 0 and (l[i-1][j] != 1 ):
                        self.graphe[(i, j)].append((i-1, j))
                    if j > 0 and (l[i][j-1] != 1):
                        self.graphe[(i, j)].append((i, j-1))
                    if i < len(l)-1 and (l[i+1][j] != 1):
                        self.graphe[(i, j)].append((i+1, j))
                    if j < len(l[0])-1 and (l[i][j+1] != 1):
                        self.graphe[(i, j)].append((i, j+1))
        
        
    def setGrille(self, i, j, val):
        self.grille[i][j] = val
            
    def getGrille(self):
        return self.grille
        
    def getGraphe(self):
        return self.graphe
        
    def getDistances(self):
        return self.d
    
    def affiche(self):
        for i in range(len(self.grille)):
            for j in range(len(self.grille[0])):
                if self.grille[i][j] == 1:
                    can.create_rectangle(j*16, i*16, j*16+16, i*16+16, fill = 'blue') # mur
                elif self.grille[i][j] == 2:
                    can.create_oval(j*16+4, i*16+4, j*16+12, i*16+12, fill = 'yellow') # gomme normale
                elif self.grille[i][j] == 3:
                    can.create_oval(j*16+2, i*16+2, j*16+14, i*16+14, fill = 'yellow') # powergum
                
    def maj_distances(self, depuis): # calcul des distances de chacun des sommets du graphe au sommet occupé par le Pac-Man ( = "depuis" )
        self.d = {sommet: inf for sommet in self.graphe}
        file = []
        
        file.append(depuis)
        self.d[depuis] = 0
        
        while file != []:
            u = file.pop(0)
            for v in self.graphe[u]:
                if self.d[v] == inf:
                    file.append(v)
                    self.d[v] = self.d[u] + 1
                elif self.d[v] > self.d[u] + 1:
                    self.d[v] = self.d[u] + 1
        
        '''
        self.d = {sommet: inf for sommet in self.graphe}
        file = []
        
        file.append(depuis)
        self.d[depuis] = 0
        
        while file != []:
            u = file.pop(0)
            for v in self.graphe[u]:
                if self.d[v] == inf:
                    file.append(v)
                    self.d[v] = self.d[u] + 1
        '''
        
        # OPTIMISATION : A* AU LIEU D'UN BFS !!!!!
    
class Pac_man:
    
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.estVivant = True  # état du Pac-Man vivant/mort
        self.direction = ''
        
    def efface(self):
        can.create_rectangle(self.x*16, self.y*16, self.x*16+16, self.y*16+16, fill = 'black')
    
    def affiche(self):
        can.create_oval(self.x*16, self.y*16, self.x*16+16, self.y*16+16, fill = 'yellow')  
        
    def move(self, event):
    
        k = event.keysym
        
        if k == 'z':
            if L.getGrille()[self.y-1][self.x] != 1:
                L.setGrille(self.y, self.x, 0)
                self.efface()
                self.y -= 1
                self.direction = 'H'
                self.affiche()
                
        elif k == 'w':
            if L.getGrille()[self.y+1][self.x] != 1:
                L.setGrille(self.y, self.x, 0)
                self.efface()
                self.y += 1
                self.direction = 'B'
                self.affiche()
   
        elif k == 'q':
            if L.getGrille()[self.y][self.x-1] != 1:
                L.setGrille(self.y, self.x, 0)
                self.efface()
                self.x = (self.x - 1)%28
                self.direction = 'G'
                self.affiche()
   
        elif k == 's':
            if L.getGrille()[self.y][self.x+1] != 1:
                L.setGrille(self.y, self.x, 0)
                self.efface()
                self.x = (self.x + 1)%27
                self.direction = 'D'
                self.affiche()

        L.maj_distances((self.y, self.x)) # mettre à jour le tableau des distances depuis la position du Pac-Man
        
        if L.getGrille()[self.y][self.x] == 3: # super pastille ?
            G1.toggleState()
            G2.toggleState()
            G3.toggleState()

class Ghost:
    
    def __init__(self, x, y, couleur):
        self.x = x
        self.y = y
        self.couleur = couleur
        self.vx = 0
        self.vy = -1
        self.estNormal = True # True = poursuit le Pac-Man / False = peut être poursuivi et mangé par le Pac-Man

    def toggleState(self):
        self.estNormal = not self.estNormal
        
    def efface(self):
        can.create_rectangle(self.x*16, self.y*16, self.x*16+16, self.y*16+16, fill = 'black')
        if L.getGrille()[self.y][self.x] == 2:
            can.create_oval(self.x*16+4, self.y*16+4, self.x*16+12, self.y*16+12, fill = 'yellow')
        elif L.getGrille()[self.y][self.x] == 3:
            can.create_oval(self.x*16+2, self.y*16+2, self.x*16+14, self.y*16+14, fill = 'yellow')
                                   
    def affiche(self):
        if self.estNormal:
            can.create_rectangle(self.x*16, self.y*16, self.x*16+16, self.y*16+16, fill = self.couleur)
        else:
            can.create_rectangle(self.x*16, self.y*16, self.x*16+16, self.y*16+16, fill = 'white')
    
    def move_bfs(self): # A REECRIRE PLUS CLAIREMENT !!!
        
        d = L.getDistances() # dictionnaire des distances dans le labyrinthe depuis la position du Pac-Man
       
        self.efface() # effacement de la position actuelle

        if self.estNormal :
            if (self.y, self.x-1) in d and d[(self.y, self.x-1)] <= d[(self.y, self.x)]: # si la case à gauche est accessible et est à une distance plus petite que la case actuelle
                self.x -= 1
            elif (self.y, self.x+1) in d and d[(self.y, self.x+1)]  <= d[(self.y, self.x)]: # case à droite 
                self.x += 1
            elif (self.y-1, self.x) in d and  d[(self.y-1, self.x)] <= d[(self.y, self.x)]: # case en dessus 
                self.y -= 1
            elif (self.y+1, self.x) in d and  d[(self.y+1, self.x)] <= d[(self.y, self.x)]: # case en dessous
                self.y += 1
        else: # mode panique
            if (self.y, self.x-1) in d and d[(self.y, self.x-1)] >= d[(self.y, self.x)]: # si la case à gauche est accessible et est à une distance plus petite que la case actuelle
                self.x -= 1
            elif (self.y, self.x+1) in d and d[(self.y, self.x+1)]  >= d[(self.y, self.x)]: # case à droite 
                self.x += 1
            elif (self.y-1, self.x) in d and  d[(self.y-1, self.x)] >= d[(self.y, self.x)]: # case en dessus 
                self.y -= 1
            elif (self.y+1, self.x) in d and  d[(self.y+1, self.x)] >= d[(self.y, self.x)]: # case en dessous
                self.y += 1
        
        self.affiche() # affichage à la nouvelle position
    
    def move_manhattan(self):
        
        self.efface() # effacement de la position actuelle
        
        if abs(self.x - P.x) + abs(self.y-1 - P.y) < abs(self.x - P.x) + abs(self.y - P.y) and L.getGrille()[self.y-1][self.x] != 1:
            self.y -= 1
        elif abs(self.x - P.x) + abs(self.y+1 - P.y) < abs(self.x - P.x) + abs(self.y - P.y) and L.getGrille()[self.y+1][self.x] != 1:
            self.y += 1 
        elif abs(self.x-1 - P.x) + abs(self.y - P.y) < abs(self.x - P.x) + abs(self.y - P.y) and L.getGrille()[self.y][self.x-1] != 1:
            self.x -= 1
        elif abs(self.x+1 - P.x) + abs(self.y - P.y) < abs(self.x - P.x) + abs(self.y - P.y) and L.getGrille()[self.y][self.x+1] != 1:
            self.x += 1
        
        self.affiche() # affichage à la nouvelle position

    def move_random(self):
        
        self.efface() # effacement de la position actuelle
        
        g = L.getGraphe() # dictionnaire des listes d'adjacence du labyrinthe
        
        if L.getGrille()[self.y+self.vy][self.x+self.vx] == 1: # arrivée à une intersection ( <=> le noeud occupé par le fantôme possède plus de 2 voisins ) ou à un mur ( '1' dans la position suivante dans la grille )
            voisin = choice(g[(self.y, self.x)]) # on tire un voisin au hasard
            self.vx = voisin[1] - self.x # calcul du nouveau sens de déplacement
            self.vy = voisin[0] - self.y # ( = différence entre les coordonnées de la position du fantôme et de celle du voisin à atteindre )
        
        
        self.x += self.vx # nouvelle coordonnées
        self.y += self.vy
        
        self.affiche() # affichage à la nouvelle position
        
# INTERFACE


def anim():
    G1.move_bfs()
    G2.move_manhattan()
    G3.move_random()
    fen.after(200, anim) 


laby = [
[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
[1,2,2,2,2,2,2,2,2,2,2,2,2,1,1,2,2,2,2,2,2,2,2,2,2,2,2,1],
[1,2,1,1,1,1,2,1,1,1,1,1,2,1,1,2,1,1,1,1,1,2,1,1,1,1,2,1],
[1,3,1,0,0,1,2,1,0,0,0,1,2,1,1,2,1,0,0,0,1,2,1,0,0,1,3,1],
[1,2,1,1,1,1,2,1,1,1,1,1,2,1,1,2,1,1,1,1,1,2,1,1,1,1,2,1],
[1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1],
[1,2,1,1,1,1,2,1,1,2,1,1,1,1,1,1,1,1,2,1,1,2,1,1,1,1,2,1],
[1,2,1,1,1,1,2,1,1,2,1,1,1,1,1,1,1,1,2,1,1,2,1,1,1,1,2,1],
[1,2,2,2,2,2,2,1,1,2,2,2,2,1,1,2,2,2,2,1,1,2,2,2,2,2,2,1],
[1,1,1,1,1,1,2,1,1,1,1,1,0,1,1,0,1,1,1,1,1,2,1,1,1,1,1,1],
[1,0,0,0,0,1,2,1,1,1,1,1,0,1,1,0,1,1,1,1,1,2,1,0,0,0,0,1],
[1,0,0,0,0,1,2,1,1,0,0,0,0,0,0,0,0,0,0,1,1,2,1,0,0,0,0,1],
[1,0,0,0,0,1,2,1,1,0,1,1,0,0,0,0,1,1,0,1,1,2,1,0,0,0,0,1],
[1,1,1,1,1,1,2,1,1,0,1,0,0,0,0,0,0,1,0,1,1,2,1,1,1,1,1,1],
[0,0,0,0,0,0,2,0,0,0,1,0,0,0,0,0,0,1,0,0,0,2,0,0,0,0,0,0],
[1,1,1,1,1,1,2,1,1,0,1,0,0,0,0,0,0,1,0,1,1,2,1,1,1,1,1,1],
[1,0,0,0,0,1,2,1,1,0,1,1,1,1,1,1,1,1,0,1,1,2,1,0,0,0,0,1],
[1,0,0,0,0,1,2,1,1,0,0,0,0,0,0,0,0,0,0,1,1,2,1,0,0,0,0,1],
[1,0,0,0,0,1,2,1,1,0,1,1,1,1,1,1,1,1,0,1,1,2,1,0,0,0,0,1],
[1,1,1,1,1,1,2,1,1,0,1,1,1,1,1,1,1,1,0,1,1,2,1,1,1,1,1,1],
[1,2,2,2,2,2,2,2,2,2,2,2,2,1,1,2,2,2,2,2,2,2,2,2,2,2,2,1],
[1,2,1,1,1,1,2,1,1,1,1,1,2,1,1,2,1,1,1,1,1,2,1,1,1,1,2,1],
[1,2,1,1,1,1,2,1,1,1,1,1,2,1,1,2,1,1,1,1,1,2,1,1,1,1,2,1],
[1,3,2,2,1,1,2,2,2,2,2,2,2,0,0,2,2,2,2,2,2,2,1,1,2,2,3,1],
[1,1,1,2,1,1,2,1,1,2,1,1,1,1,1,1,1,1,2,1,1,2,1,1,2,1,1,1],
[1,1,1,2,1,1,2,1,1,2,1,1,1,1,1,1,1,1,2,1,1,2,1,1,2,1,1,1],
[1,2,2,2,2,2,2,1,1,2,2,2,2,1,1,2,2,2,2,1,1,2,2,2,2,2,2,1],
[1,2,1,1,1,1,1,1,1,1,1,1,2,1,1,2,1,1,1,1,1,1,1,1,1,1,2,1],
[1,2,1,1,1,1,1,1,1,1,1,1,2,1,1,2,1,1,1,1,1,1,1,1,1,1,2,1],
[1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1],
[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]]


        
fen = Tk()
fen.title('Pacman')

can = Canvas(fen, width = 1280, height = 800, background='black')
can.pack()

# création et affichage du labyrinthe
L = Lab(laby)
L.affiche()

# création et affichage du Pac-Man
P = Pac_man(1, 1)
P.affiche()
L.maj_distances((P.y, P.x)) # première mise à jour des distances dans le labyrinthe depuis la position du Pac-Man

fen.bind('<Any-KeyPress>', P.move) # liaison des évènements clavier à la méthode move() de l'instance du Pac-Man



# créaction du fantôme, et déplacement toutes les 1 s
G1 = Ghost(15, 12, 'cyan')
G2 = Ghost(15, 13, 'pink')
G3 = Ghost(15, 14, 'red')
anim()


fen.mainloop()
