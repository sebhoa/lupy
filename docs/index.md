hide:
    - toc
    - navigation

---

# Bienvenue !

Vous trouverez ici des idées ludiques pour faire programmer vos étudiant-es.

Pour participer, il suffit de se rendre sur le _repo_ [gitlab du projet](https://gitlab.com/sebhoa/lupy), de le cloner puis de proposer son idée, dans un répertoire dédié, dans une des quatre rubriques existantes :

- :fontawesome-solid-dice: Petits jeux
- :fontawesome-solid-infinity: Énigmes mathématiques
- :fontawesome-solid-dungeon: Autres énigmes et puzzles
- :fontawesome-solid-scroll: Projets