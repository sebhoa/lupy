# La constante de Kaprekar

## Le petit jeu

1. Choisissez un nombre à 4 chiffres **pas tous égaux**. 
2. À partir de ce nombre en créer 2 autres :
    - un petit $p$ : en rangeant les chiffres de votre premier nombre dans l'ordre croissant
    - un grand $g$ : en rangeant ces mêmes chiffres dans l'ordre inverse. 
3. Calculer $g - p$ et recommencer à l'étape 2.

Note : si le nombre est **6174** le processus devient infini.

## But du programme Python

Montrer que quelque soit le nombre choisi (qui respecte la contrainte des 4 chiffres non tous égaux), on tombe sur 6174 en au plus 7 étapes.

**6174** est appelée [constante de Kaprekar](https://en.wikipedia.org/wiki/6174)



