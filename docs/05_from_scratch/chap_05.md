**CHAPITRE 5**

--- 

# Dessiner

Il est temps de dessiner des carrés pour visualiser notre serpent. Pygame offre une méthode `draw.rect` pour dessiner des rectangles. La syntaxe est la suivante :

```python
pygame.draw.rect(surface, couleur, rectangle)
```

Où :

- _surface_ est la surface où dessiner (pour nous, ce sera l'`ecran`)
- _couleur_ est une couleur nous y reviendrons
- _rectangle_ est un rectangle pygame qu'il va falloir construire


## Des objets Pygame courants

### Un rectangle

Pygame offre un objet `Rect` qui se construit par `pygame.Rect` et en fournissant les coordonnées du point supérieur gauche ainsi que les dimensions : `pygame.Rect(x, y, largeur, hauteur)`. 

### Une couleur

Il existe un objet `Color` de pygame qui construit une couleur si on lui passe un triplet _(R, V, B)_ (les valeurs décimales entre 0 et 255 représentant des quantités de rouge, de vert et de bleu) ou bien un quadruplet : la 4e composante étant la transparence.

Par exemple, `pygame.Color(0, 0, 0)` correspond à la couleur noir. Depuis pygame 2.0 on peut aussi donner le nom d'une couleur (une simple chaine de caractères). Un tableau des couleurs est disponible sur [htmlcolorcodes](https://htmlcolorcodes.com/fr/noms-de-couleur/)

 On peut aussi, par souci de compatibilité, créer une couleur par `pygame.Color` en lui passant un nom.

Voici les couleurs que nous allons utiliser (à mettre dans `constantes.py` bien sûr) :

```python
COULEUR_TETE = pygame.Color('lightcoral')
COULEUR_CORPS = pygame.Color('lightslategrey')
COULEUR_FOND = pygame.Color('sienna')
```

## Le serpent prend forme

Comment notre serpent se dessine-t-il ? C'est assez simple : parcourir chacune des coordonnées du tableau de positions, calculer les coordonnées en pixels correspondantes et dessiner un carré de `SIZE` x `SIZE` à cet endroit.

```python
class Serpent:
    ...
    def se_dessine(self):
        for coords in self.pos:
            px, py = xy_vers_pixels(coords)
            pygame.draw.rect(self.ecran, COULEUR_CORPS, pygame.Rect(px, py, SIZE, SIZE))
        px, py = xy_vers_pixels(self.pos[self.tete]) 
        pygame.draw.rect(self.ecran, COULEUR_TETE, pygame.Rect(px, py, SIZE, SIZE))
```

`xy_vers_pixels` est une fonction à qui on passe un couple d'entiers, correspondants au numéro de ligne et de colonne d'un carré et qui calcule les coordonnées en pixels du coin supérieur gauche de ce carré.

??? info "Les concepts à revoir"

    1. Définition d'une fonction
    2. Appel d'une fonction

Dès lors, le `Jeu`, dans sa boucle va demander à l'Arène (son interlocuteur) de se dessiner... et l'Arène s'exécute en demandant à son serpent de se dessiner :

```python
class Jeu:
    ...
    def loop(self):
        fini = False
        while not fini:
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    fini = True
            self.arene.se_dessine()
        pygame.quit()
```

Et du côté de l'Arène :

```python
class Arene:
    ...
    def se_dessine(self):
        self.serpent.se_dessine()
```

Si vous avez suivi et réalisé l'ensemble du code en l'exécutant vous avez constaté que l'écran reste noir. Il nous manque quelque chose : en présence d'interfaces graphiques, il faut explicitement gérer les rafraîchissements de l'écran dès qu'une action de dessin est faite.

Ceci se fait par `pygame.display.flip()`. 

## Exercice

!!! question "À faire vous-même"

    === "Énoncé"

        Dans un fichier `snake_07.py`, faites évoluer votre jeu depuis le fichier précédent pour :

        1. Faire afficher le serpent ;
        2. Dans la partie `start`, avant de lancer la boucle de jeu, changer la couleur de fond en utilisant 

            ```python
            self.ecran.fill(COULEUR_FOND)
            ```
        3. Chercher dans la documentation de Pygame, comment changer le titre de la fenêtre. Voici ce que vous devriez obtenir :

            ![snake 01](images/snake_07.png){ .centrer width=70% }

    === "Solution"

        ```python
        --8<-- "05_from_scratch/scripts/snake_07.py"
        ```