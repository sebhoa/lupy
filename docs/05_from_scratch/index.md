---
hide: toc
tag:
    - retro gaming
    - snake
--- 

# Initiation à Python, par le jeu et Pygame

![anime](images/snake_final.gif){ align=left width=35% } 

Le projet original est de réaliser une introduction **complète**[^1] à la programmation en utilisant Python3 et la Programmation Orientée Objet, en partant de rien !

!!! warning "Projet en construction !"

    Ce projet est en construction. Cette première étape présente une sorte de tutoriel de prise en main du module Pygame. Mais toutes les pages connexes pour s'initier à la programmation Python n'existent pas encore. Dans les zones "Les concepts", parfois existent des liens vers des pages mais le projet final vsise à refaire toutes ces entrées.

Ces pages offrent deux niveaux de lecture :

1. On peut les parcourir de façon linéaire comme un tutoriel d'initiation à Pygame. Si on a déjà de bonnes bases en Python par exemple. Mais même si on est débutant, on peut juste copier et exécuter les codes fournis, en sachant que bien des choses seront incompréhensibles.

2. On peut aussi s'arrêter aux points d'apprentissage des concepts sous-jacents et les étudier plus ou moins profondément avant de continuer.

Les termes en **gras** seront des termes important de vocabulaire.

## La _timeline_ d'apprentissage

![timeline](images/timeline.svg)

## Quel est le support ?

- Un fil rouge : la réalisation d'un [jeu de _snake_](https://fr.wikipedia.org/wiki/Snake_(genre_de_jeu_vid%C3%A9o)) en utilisant la bibliothèque [pygame](https://devdocs.io/pygame/)
- Prérequis : **aucun** ! Et oui, c'est le challenge.
- Nous allons introduire les concepts au fur et à mesure.

## Autres ressources Pygame 

- [Le site officiel](https://github.com/pygame/pygame)
- [Une doc complète](https://devdocs.io/pygame/)
- [Un tutoriel](https://pygame.readthedocs.io/en/latest/#)

[^1]: Ce que j'entends par _complète_ c'est l'introduction des concepts de base de la programmation : types et valeurs de base, instruction, affectation, structures plus complexes comme les listes/tableaux, tuples, dictionnaires etc. la notion de fonctions (définitions et appels), ...
