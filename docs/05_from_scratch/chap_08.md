**CHAPITRE 8**

--- 

# Finaliser le jeu

- Gérer les collisions
- Ajouter un score
- Ajouter un _timer_

## Le concept de _mode_

Lorsque le serpent entre en collision avec un des bords de l'arène ou avec lui-même, le jeu prend fin. Jusqu'à présent nous ne pouvons quitter le jeu qu'en utilisant la touche `Q` de notre clavier. Même le bouton de fermeture de la fenêtre est inactif.

Si le jeu se termine à cause d'une collision, il n'est pas souhaitable que tout s'arrête brutalement, avec fermeture de la fenêtre. En effet, le joueur voudra probablement voir son score, éventuellement avoir accès à un menu pour recommencer une partie etc.

Basculer dans un mode _stop_ serait approprié. Dans ce mode on pourrait avoir l'affichage d'un menu par exemple. On en tout cas, dans sa version la plus simple, le mode _stop_ attend tout simplement que l'utilisateur décide de quitter le jeu.

### Différents modes 

Nous pourrions profiter de ce concept pour créer un mode _start_ qui serait celui dans lequel le jeu démarrerait, empêchant ainsi le serpent de commencer à se mouvoir sans l'intervention du joueur (vous avez remarqué que le serpent se _replie_ sur lui-même au tout début ?), empêchant le _timer_ de se lancer, etc. 

Un mode _pause_ pourrait être activé pendant la partie sur décision du joueur (un appui sur la barre `espace` par exemple) : dans ce mode, le serpent ne bouge pas mais le _timer_ continue de défiler

Le mode _move_ ou mode normal serait celui de la partie qui se déroule, avec le serpent qui bouge.

Enfin le mode _stop_ que nous avons déjà évoqué.


### Ajout d'un mode au `Jeu`

Les constantes de modes sont :

```python
MODE_START = 0 # le serpent ne bouge pas
MODE_MOVE = 1 # mode normal, le serpent bouge
MODE_PAUSE = 2 # le joueur a mis le jeu en pause
MODE_STOP = 3
``` 

Et l'impact sur le contrôleur :

```python
class Jeu:

    def __init__(self):
        ...
        self.mode = MODE_START 

    def gerer_event(self, event):
        if event.type == pygame.KEYDOWN:
            if self.mode == MODE_START:
                self.debut = int(time.time())
                self.mode = MODE_MOVE
            if self.mode != MODE_STOP:
                self.move_events(event)
            elif event.key == pygame.K_q:
                self.fini = True
        elif # ici une condition pour savoir qu'un crash a eu lieu 
             and self.mode == MODE_MOVE:
            self.game_over()
            self.mode = MODE_STOP
        elif event.type == pygame.QUIT:
            self.fini = True
``` 

## Les collisions

Dès que le serpent a bougé, il doit vérifier qu'il n'a pas tenté de sortir de l'arène, ni heurté son propre corps. Si c'est le cas, il faut avertir le jeu (le contrôleur) que la partie est terminée (basculer en mode _stop_).

Le souci : le Serpent ne connaît que l'Arène, pas le Jeu. Alors nous pourrions faire une succession de renvois de valeur pour signifier au Jeu, par l'intermédiaire de l'Arène que la partie est finie.

Cette façon de faire est artificielle et surtout, pourrait être très fastidieuse si les intermédiaires entre l'objet qui doit faire l'annonce et le contrôleur s'avéraient nombreux (ce qui peut être le cas dans des projets plus importants).

Les événements utilisateur sont là pour ça. 

### Les événements utilisateur

Lorsque le contrôleur teste le type d'un événement avec `event.type == pygame.KEYDOWN`, la constante `pygame.KEYDOWN` est un simple numéro identifiant un événement prédéfini ; ici le fait qu'une touche du clavier a été enfoncée.

Il existe une constante `pygame.USEREVENT` qui est un identifiant libre, que l'on peut utiliser. Si on a besoin d'un deuxième événement utilisateur, il suffit d'incrémenter cette valeur : `pygame.USEREVENT + 1`.

Ainsi, dans les constantes nous allons déclarer un événement _crash_ :

```python
CRASH_EVENT = pygame.USERVENT
```

Ensuite, il suffira au serpent de _déclencher_ cet événement lorsqu'il aura détecté une collision. Avant de détailler cela, nous pouvons compléter la partie manquante de notre méthode `gerer_event` :

```python
class Jeu:
    ...

    def gerer_event(self, event):
        if event.type == pygame.KEYDOWN:
            if self.mode == MODE_START:
                self.debut = int(time.time())
                self.mode = MODE_MOVE
            if self.mode != MODE_STOP:
                self.move_events(event)
            elif event.key == pygame.K_q:
                self.fini = True
        elif event.type == CRASH_EVENT and self.mode == MODE_MOVE:
            self.game_over()
            self.mode = MODE_STOP
        elif event.type == pygame.QUIT:
            self.fini = True
``` 

!!! note "Note"

    La méthode `game_over` pourra afficher un message au milieu de l'écran et attendre que le joueur quitte le jeu.

### Du côté du Serpent

Il faut déjà vérifier la collision éventuelle :

#### Le serpent se mord ?

```python
class Serpent:
    ...

    def se_mord(self):
        return any(self.pos[i] == self.pos[self.tete] 
                              for i in range(len(self.pos)-1))
```

??? info "Les concepts"

    - Les itérateurs
    - Les fonctions qui les utilisent : `sum`, `max`, `sorted`, `all`, `any`...


#### Le serpent est dans l'arène ?

```python
class Serpent:
    ...
    def dans_arene(self):
        """Renvoie True si le serpent est bien dans l'arène"""
        tete_x, tete_y = self.pos[self.tete]
        return 0 <= tete_x < COLS and 0 <= tete_y < ROWS
```

#### Le crash ?

```python
class Serpent:
    ...

    def crash(self):
        """Renvoie True si le serpent s'est crashé qq part"""
        if self.se_mord() or not self.dans_arene():
            self.declenche_event(CRASH_EVENT)
            return True
        return False
```

Le déclenchement d'un _event_ se fait en deux temps :

1. création d'un objet événement avec le bon identifiant : `pygame.event.Event(event_id)`
2. mettre l'événement dans le buffer des événements : `pygame.event.post(event)`

```python
class Serpent:
    ...

    def declencher_event(self, event_id):
        pygame.event.post(pygame.event.Event(event_id))
```

## Exercice

!!! question "À faire vous-même"

    === "Énoncé"

        Intégrer la gestion des crashes dans une version `snake_10.py`. Pour le `game_over` pour l'instant fait juste un `pass`

    === "Solution"

        ```python
        --8<-- "05_from_scratch/scripts/snake_10.py"
        ```


## Ajout d'un score

En nous servant du principe de l'événement utilisateur, nous allons ajouter un score au jeu : propriété initialisée à 0, à chaque pomme mangée, ce score augmente de 1.

Il nous faut utiliser les objets texte de pygame pour afficher des informations à l'écran.

### Les textes de Pygame

Probablement un des points faibles de Pygame[^1]. Pour afficher un texte il faut :

1. Définir une fonte,
2. faire un rendu de cette fonte sur un texte : ce qui donne une image
3. afficher cette image

Heureusement, il est possible d'utiliser des valeurs par défaut rendant le processus un peu moins pénible. L'étape 1 se fait au démarrage, avec une fonte par défaut présente sur le système (c'est la signification de l'argument `None`) :

```python
class Jeu:
    ...
    def start(self):
        pygame.init()
        self.font = pygame.font.SysFont(None, 24)
        ...
```

Les étapes 2 et 3 se font au moment de l'affichage :

```python
class Jeu:
    ...
    def infos(self):
        score = self.font.render(f'{self.score:03}', True, COULEUR_TEXTE)
        self.ecran.blit(score, POSITION_SCORE)
``` 

La méthode `blit` permet d'[afficher une image](https://pygame.readthedocs.io/en/latest/3_image/image.html).

### La propriété `score`

Elle fait partie des propriétés de l'objet `Jeu` :

```python
class Jeu:
    ...
    def __init__(self):
        ...
        self.score = 0
```

et la mise à jour se fait par événement utilisateur :

```python
SCORE_EVENT = pygame.USEREVENT + 1
```

qui sera déclenché par le Serpent :

```python
class Serpent:
    ...

    def bouge(self):
        x, y = self.pos[self.tete]
        dx, dy = self.direction
        self.pos.append((x+dx, y+dy))
        self.pos.pop(0)
        if not self.crash():
            if self.arene.une_pomme():
                self.declencher_event(SCORE_EVENT)
                self.mange()
```

et intercepté par le contrôleur :

```python
class Jeu:
    ...
    def gerer_event(self, event):
        ...
        elif event.type == SCORE_EVENT:
            self.score += 1
        ...
```

## Exercice final

!!! question "À faire vous-même"

    === "Énoncé"

        Réaliser la version finale du SNAKE en intégrant :
        
        1. les modes et les collisions
        2. le score
        3. le _timer_
        4. on pourra aussi ajouter plsu d'aléatoire sur l'apparition des pommes, faire en sorte que les pommes disparaissent au bout d'un temps aléatoire aussi, faire accélérer régulièrement le serpent...
        
    === "Solution"

        ```python
        --8<-- "05_from_scratch/scripts/snake.py"
        ```




[^1]: En réalite cela signifie que si on a beaucoup de textes à gérer le mieux est de créer un objet texte pour encapsuler les différentes étapes.