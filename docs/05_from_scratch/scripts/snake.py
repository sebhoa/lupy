import pygame
import random
from constantes import *

def xy_vers_pixels(coords):
    x, y = coords
    return x * SIZE, y * SIZE

class Pomme:
    
    def __init__(self, pos, arene, date_peremption):
        self.arene = arene
        self.ecran = arene.ecran
        self.pos = pos
        self.date_peremption = date_peremption

    def se_dessine(self):
        px, py = xy_vers_pixels(self.pos)
        pygame.draw.rect(self.ecran, COULEUR_POMME, pygame.Rect(px, py, SIZE, SIZE))

class Serpent:
    def __init__(self, arene):
        self.arene = arene
        self.ecran = arene.ecran
        self.pos = [(COLS//2 + i, ROWS//2) for i in range(-LENGTH//2, LENGTH//2)]
        self.tete = -1
        self.direction = 0, 0

    def change_direction(self, dx, dy):
        self.direction = dx, dy

    def mange(self):
        pos_queue = self.pos[0]
        for _ in range(CROISSANCE):
            self.pos.insert(0, pos_queue)

    def declenche_event(self, event_id):
        pygame.event.post(pygame.event.Event(event_id))

    def se_mord(self):
        """Renvoie True si la position de la tête se retrouve à une des positions du corps, False sinon"""
        return any(self.pos[i] == self.pos[self.tete] for i in range(len(self.pos)-1))

    def dans_arene(self):
        """Renvoie True si le serpent est bien dans l'arène"""
        tete_x, tete_y = self.pos[self.tete]
        return 0 <= tete_x < COLS and 0 <= tete_y < ROWS

    def crash(self):
        """Renvoie True si le serpent s'est crashé qq part"""
        if self.se_mord() or not self.dans_arene():
            self.declenche_event(CRASH_EVENT)
            return True
        return False

    def bouge(self):
        x, y = self.pos[self.tete]
        dx, dy = self.direction
        self.pos.append((x+dx, y+dy))
        self.pos.pop(0)
        if not self.crash():
            if self.arene.une_pomme():
                self.declenche_event(SCORE_EVENT)
                self.mange()

    def se_dessine(self):
        for coords in self.pos:
            px, py = xy_vers_pixels(coords)
            pygame.draw.rect(self.ecran, COULEUR_CORPS, pygame.Rect(px, py, SIZE, SIZE))
        px, py = xy_vers_pixels(self.pos[self.tete]) 
        pygame.draw.rect(self.ecran, COULEUR_TETE, pygame.Rect(px, py, SIZE, SIZE))


class Arene:
    def __init__(self, jeu):
        self.jeu = jeu
        self.ecran = jeu.ecran
        self.serpent = Serpent(self)
        self.pommes = {}
        self.date = jeu.time()

    def anime(self):
        self.gestion_pommes()
        self.serpent.bouge()

    def serpent_change_direction(self, dx, dy):
        self.serpent.change_direction(dx, dy)

    def retrait_pomme(self, pos):
        self.pommes.pop(pos)

    def ajout_pomme(self, date_peremption):
        pos = self.random_position()
        self.pommes[pos] = Pomme(pos, self, date_peremption)

    def une_pomme(self):
        position_serpent = self.serpent.pos[self.serpent.tete]
        if position_serpent in self.pommes:
            self.retrait_pomme(position_serpent)
            return True
        return False

    def random_position(self):
        return random.randrange(COLS), random.randrange(ROWS)

    def retire_pommes(self):
        """retire les pommes périmées"""
        date = self.jeu.time()
        for pos in list(self.pommes.keys()):
            pomme = self.pommes[pos]
            if pomme.date_peremption < date:
                self.retrait_pomme(pos)

    def gestion_pommes(self):
        date = self.jeu.time()
        delai_apparition = random.randint(*DELAI_POMME)
        delai_peremption = random.randint(*VALIDITE)
        if (date - self.date) > delai_apparition:
            self.ajout_pomme(date + delai_peremption)
            self.date = date
        self.retire_pommes()

    def se_dessine(self):
        self.serpent.se_dessine()
        for pomme in self.pommes.values():
            pomme.se_dessine()
    
    def reset(self):
        self.serpent = Serpent(self)
        self.pommes = {}
        self.date = self.jeu.time()


class Jeu:
    def __init__(self):
        self.ecran = pygame.display.set_mode((LARGEUR, HAUTEUR)) 
        self.arene = Arene(self)
        self.fini = False
        self.mode = MODE_START
        self.debut = None # sera initialisé au moment du lancement  
        self.score = 0
        self.font = None
        self.delai = DELAI_MS
        self.deja_accelere = False

    def start(self):
        pygame.init()
        pygame.display.set_caption('Another SNAKE game...')
        self.font = pygame.font.SysFont(None, 24)
        self.ecran.fill(COULEUR_FOND)
        self.loop()

    def move_events(self, event):
        if event.key in DIRECTIONS:
            dx, dy = DIRECTIONS[event.key]
            self.arene.serpent_change_direction(dx, dy)
        elif event.key == pygame.K_SPACE:
            self.mode = 3 - self.mode # bascule de PAUSE à MOVE et vice-versa
        elif event.key == pygame.K_q:
            self.fini = True

    def time(self):
#        return int(time.time())
        return pygame.time.get_ticks() // 1000

    def gerer_event(self, event):
        if event.type == pygame.KEYDOWN:
            if self.mode == MODE_START:
                self.debut = self.time()
                self.mode = MODE_MOVE
            if self.mode != MODE_STOP:
                self.move_events(event)
            elif event.key == pygame.K_q:
                self.fini = True
            elif event.key == pygame.K_r:
                self.reset()
                self.mode = MODE_START
        elif event.type == SCORE_EVENT:
            self.score += 1
        elif event.type == CRASH_EVENT and self.mode == MODE_MOVE:
            self.game_over()
            self.mode = MODE_STOP
        elif event.type == pygame.QUIT:
            self.fini = True

    def accelere(self):
        if self.temps_ecoule() % DELAI_ACC == 0 and not self.deja_accelere:
            self.deja_accelere = True
            self.delai -= DELTA_DELAI
            print(self.delai)
        elif self.temps_ecoule() % DELAI_ACC != 0:
            self.deja_accelere = False

    def temps_ecoule(self):
        return 0 if self.debut is None else self.time() - self.debut
        
    def infos(self):
        ecoule = self.temps_ecoule()
        temps = self.font.render(f'{ecoule//60:02}:{ecoule%60:02}', True, COULEUR_TEXTE)
        score = self.font.render(f'{self.score:03}', True, COULEUR_TEXTE)
        self.ecran.blit(temps, POSITION_TEMPS)
        self.ecran.blit(score, POSITION_SCORE)

    def game_over(self):
        big = pygame.font.SysFont(None, 36)
        fin = big.render('GAME OVER', True, COULEUR_TEXTE)
        self.ecran.blit(fin, CENTRE)
        self.refresh_and_pause()

    def effacer(self):
        self.ecran.fill(COULEUR_FOND)

    def refresh_and_pause(self):
        pygame.display.flip()
        pygame.time.delay(DELAI_MS)


    def loop(self):
        while not self.fini:
            for event in pygame.event.get():
                self.gerer_event(event)
            if self.mode != MODE_STOP:
                if self.delai > DELAI_MIN:
                    self.accelere()
                self.effacer()
                if self.mode == MODE_MOVE:
                    self.arene.anime()
                self.arene.se_dessine()
                self.infos()
                self.refresh_and_pause()
        pygame.quit()    

    def reset(self):
        self.arene.reset()
    
snake_game = Jeu()
snake_game.start()

