import pygame
import random
import time
from constantes import *

def xy_vers_pixels(coords):
    x, y = coords
    return x * SIZE, y * SIZE

class Pomme:
    
    def __init__(self, pos, arene):
        self.arene = arene
        self.ecran = arene.ecran
        self.pos = pos

    def se_dessine(self):
        px, py = xy_vers_pixels(self.pos)
        pygame.draw.rect(self.ecran, COULEUR_POMME, pygame.Rect(px, py, SIZE, SIZE))

class Serpent:
    def __init__(self, arene):
        self.arene = arene
        self.ecran = arene.ecran
        self.pos = [(COLS//2 + i, ROWS//2) for i in range(-LENGTH//2, LENGTH//2)]
        self.tete = -1
        self.direction = 0, 0

    def change_direction(self, dx, dy):
        self.direction = dx, dy

    def mange(self):
        pos_queue = self.pos[0]
        for _ in range(CROISSANCE):
            self.pos.insert(0, pos_queue)

    def bouge(self):
        x, y = self.pos[self.tete]
        dx, dy = self.direction
        self.pos.append((x+dx, y+dy))
        if self.arene.une_pomme():
            self.mange()
        self.pos.pop(0)

    def se_dessine(self):
        for coords in self.pos:
            px, py = xy_vers_pixels(coords)
            pygame.draw.rect(self.ecran, COULEUR_CORPS, pygame.Rect(px, py, SIZE, SIZE))
        px, py = xy_vers_pixels(self.pos[self.tete]) 
        pygame.draw.rect(self.ecran, COULEUR_TETE, pygame.Rect(px, py, SIZE, SIZE))


class Arene:
    def __init__(self, jeu):
        self.jeu = jeu
        self.ecran = jeu.ecran
        self.serpent = Serpent(self)
        self.pommes = {}
        self.date = pygame.time.get_ticks()

    def anime(self):
        self.gestion_pommes()
        self.serpent.bouge()

    def serpent_change_direction(self, dx, dy):
        self.serpent.change_direction(dx, dy)

    def une_pomme(self):
        position_serpent = self.serpent.pos[self.serpent.tete]
        if position_serpent in self.pommes:
            self.pommes.pop(position_serpent)
            return True
        return False

    def random_position(self):
        return random.randrange(COLS), random.randrange(ROWS)
    
    def ajoute_pomme(self):
        pos = self.random_position()
        self.pommes[pos] = Pomme(pos, self)

    def gestion_pommes(self):
        date = pygame.time.get_ticks()
        if date - self.date > DELAI_POMME:
            self.ajoute_pomme()
            self.date = date

    def se_dessine(self):
        self.serpent.se_dessine()
        for pomme in self.pommes.values():
            pomme.se_dessine()


class Jeu:
    def __init__(self):
        self.ecran = pygame.display.set_mode((LARGEUR, HAUTEUR)) 
        self.arene = Arene(self)
        self.fini = False   

    def start(self):
        pygame.init()
        pygame.display.set_caption('Another SNAKE game...')
        self.ecran.fill(COULEUR_FOND)
        self.loop()

    def gerer_event(self, event):
        if event.type == pygame.KEYDOWN:
            if event.key in DIRECTIONS:
                dx, dy = DIRECTIONS[event.key]
                self.arene.serpent_change_direction(dx, dy)
            elif event.key == pygame.K_q:
                self.fini = True

    def pause(self):
        pygame.time.delay(DELAI_MS)

    def effacer(self):
        self.ecran.fill(COULEUR_FOND)

    def loop(self):
        while not self.fini:
            for event in pygame.event.get():
                self.gerer_event(event)
            self.pause()
            self.effacer()
            self.arene.anime()
            self.arene.se_dessine()
            pygame.display.flip()
        pygame.quit()    
    
snake_game = Jeu()
snake_game.start()

