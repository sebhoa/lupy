import pygame
from constantes import *

def xy_vers_pixels(coords):
    x, y = coords
    return x * SIZE, y * SIZE

class Serpent:
    def __init__(self, arene):
        self.arene = arene
        self.ecran = arene.ecran
        self.pos = [(COLS//2 + i, ROWS//2) for i in range(-LENGTH//2, LENGTH//2)]
        self.tete = -1

    def se_dessine(self):
        for coords in self.pos:
            px, py = xy_vers_pixels(coords)
            pygame.draw.rect(self.ecran, COULEUR_CORPS, pygame.Rect(px, py, SIZE, SIZE))
        px, py = xy_vers_pixels(self.pos[self.tete]) 
        pygame.draw.rect(self.ecran, COULEUR_TETE, pygame.Rect(px, py, SIZE, SIZE))        

class Arene:
    def __init__(self, jeu):
        self.jeu = jeu
        self.ecran = jeu.ecran
        self.serpent = Serpent(self)

    def se_dessine(self):
        self.serpent.se_dessine()

class Jeu:
    def __init__(self):
        self.ecran = pygame.display.set_mode((LARGEUR, HAUTEUR)) 
        self.arene = Arene(self)   

    def start(self):
        pygame.init()
        pygame.display.set_caption('Another SNAKE game...')
        self.ecran.fill(COULEUR_FOND)
        self.loop()

    def loop(self):
        fini = False
        while not fini:
            for event in pygame.event.get():
                if event.type in (pygame.KEYDOWN, pygame.QUIT):
                    fini = True
            self.arene.se_dessine()
            pygame.display.flip()
        pygame.quit()    
    
snake_game = Jeu()
snake_game.start()