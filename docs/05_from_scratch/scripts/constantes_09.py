import pygame

# CONSTANTES POUR LA VUE

LARGEUR = 600
HAUTEUR = 450
SIZE = 10
LENGTH = 4

COLS = LARGEUR // SIZE
ROWS = HAUTEUR // SIZE

DELAI_MS = 100

COULEUR_TETE = pygame.Color('lightcoral')
COULEUR_CORPS = pygame.Color('lightslategrey')
COULEUR_FOND = pygame.Color('sienna')
COULEUR_POMME = pygame.Color('greenyellow')


# CONSTANTES POUR LE MODÈLE

DIRECTIONS = {pygame.K_DOWN: (0, 1), 
              pygame.K_UP: (0, -1),
              pygame.K_LEFT: (-1, 0),
              pygame.K_RIGHT: (1, 0)}

DELAI_POMME = 10

CROISSANCE = 4