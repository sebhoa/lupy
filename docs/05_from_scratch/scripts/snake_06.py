import pygame
from constantes import *


class Jeu:
    def __init__(self):
        self.ecran = pygame.display.set_mode((LARGEUR, HAUTEUR)) 

    def start(self):
        pygame.init()
        self.loop()

    def loop(self):
        fini = False
        while not fini:
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    fini = True
                if event.type == pygame.MOUSEBUTTONDOWN:
                    print('Vous avez cliqué')
        self.stop()
    
    def stop(self):
        print('Au revoir')
        pygame.quit()    
    
snake_game = Jeu()
snake_game.start()