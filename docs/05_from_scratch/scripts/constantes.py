import pygame

# CONSTANTES POUR LA VUE

LARGEUR = 600
HAUTEUR = 450
SIZE = 10
LENGTH = 4

TOP_MARGE = 5
POSITION_TEMPS = LARGEUR - 50, TOP_MARGE
POSITION_SCORE = 10, TOP_MARGE
CENTRE = LARGEUR // 2 - 70, HAUTEUR // 2 - 40

COLS = LARGEUR // SIZE
ROWS = HAUTEUR // SIZE

DELAI_MS = 100  # délai initial
DELTA_DELAI = 5 # le délai diminue de 5ms 
DELAI_ACC = 20  # toutes les 20s
DELAI_MIN = 30  # sans descendre en dessous de 30ms

COULEUR_TETE = pygame.Color('lightcoral')
COULEUR_CORPS = pygame.Color('lightslategrey')
COULEUR_FOND = pygame.Color('sienna')
COULEUR_POMME = pygame.Color('greenyellow')
COULEUR_TEXTE = pygame.Color('darkred')

# EVENTS

CRASH_EVENT = pygame.USEREVENT
SCORE_EVENT = pygame.USEREVENT + 1


# CONSTANTES POUR LE MODÈLE

DIRECTIONS = {pygame.K_DOWN: (0, 1), 
              pygame.K_UP: (0, -1),
              pygame.K_LEFT: (-1, 0),
              pygame.K_RIGHT: (1, 0)}

DELAI_POMME = 3, 6 # une pomme apparait entre 3 et 6 secondes 
VALIDITE = 10, 15 # les pommes pourrissent au bout d'un délai variable de 10 à 15s

CROISSANCE = 4

MODE_START = 0 # le serpent ne bouge pas
MODE_MOVE = 1 # mode normal, le serpent bouge
MODE_PAUSE = 2 # le joueur a mis le jeu en pause
MODE_STOP = 3