import pygame
from constantes import *

class Jeu:
    
    def __init__(self):
        self.ecran = pygame.display.set_mode((LARGEUR, HAUTEUR))
    
    def start(self):
        pygame.init()
        self.loop()

    def loop(self):
        fini = False
        while not fini:
            for event in pygame.event.get():
                if event.type == pygame.QUIT: 
                    fini = True
    
mon_jeu = Jeu()
mon_jeu.start()