import pygame
from constantes import *

def xy_vers_pixels(coords):
    x, y = coords
    return x * SIZE, y * SIZE

class Serpent:
    def __init__(self, arene):
        self.arene = arene
        self.ecran = arene.ecran
        self.pos = [(COLS//2 + i, ROWS//2) for i in range(-LENGTH//2, LENGTH//2)]
        self.tete = -1
        self.direction = 0, 0

    def change_direction(self, dx, dy):
        self.direction = dx, dy

    def bouge(self):
        x, y = self.pos[self.tete]
        dx, dy = self.direction
        self.pos.append((x+dx, y+dy))
        self.pos.pop(0)

    def se_dessine(self):
        for coords in self.pos:
            px, py = xy_vers_pixels(coords)
            pygame.draw.rect(self.ecran, COULEUR_CORPS, pygame.Rect(px, py, SIZE, SIZE))
        px, py = xy_vers_pixels(self.pos[self.tete]) 
        pygame.draw.rect(self.ecran, COULEUR_TETE, pygame.Rect(px, py, SIZE, SIZE))


class Arene:
    def __init__(self, jeu):
        self.jeu = jeu
        self.ecran = jeu.ecran
        self.serpent = Serpent(self)

    def anime(self):
        self.serpent.bouge()

    def serpent_change_direction(self, dx, dy):
        self.serpent.change_direction(dx, dy)

    def se_dessine(self):
        self.serpent.se_dessine()

class Jeu:
    def __init__(self):
        self.ecran = pygame.display.set_mode((LARGEUR, HAUTEUR)) 
        self.arene = Arene(self)
        self.fini = False   

    def start(self):
        pygame.init()
        pygame.display.set_caption('Another SNAKE game...')
        self.ecran.fill(COULEUR_FOND)
        self.loop()

    def gerer_event(self, event):
        if event.type == pygame.KEYDOWN:
            if event.key in DIRECTIONS:
                dx, dy = DIRECTIONS[event.key]
                self.arene.serpent_change_direction(dx, dy)
            elif event.key == pygame.K_q:
                self.fini = True

    def pause(self):
        pygame.time.delay(DELAI_MS)

    def effacer(self):
        self.ecran.fill(COULEUR_FOND)

    def loop(self):
        while not self.fini:
            for event in pygame.event.get():
                self.gerer_event(event)
            self.pause()
            self.effacer()
            self.arene.anime()
            self.arene.se_dessine()
            pygame.display.flip()
        pygame.quit()    
    
snake_game = Jeu()
snake_game.start()