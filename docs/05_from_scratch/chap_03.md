**CHAPITRE 3**

--- 

# Gérer les événements

## Présentation

Les événements sont la base des interfaces Humain-Machine (IHM)[^1] : frappe au clavier, mouvement ou clic de souris etc. ils sont stockés dans une file d'attente appelée _buffer_. Ci-dessous un exemple de _buffer_ qui contient 5 événements :

![buffer d'événements](images/buffer_events.png){ .centrer }

pygame nous offre le moyen de parcourir ce buffer pour y récupérer les _events_ :

1. `pygame.event.get()` : l'accès au buffer
2. la boucle `for` 

??? info "Les concepts"

    1. [la boucle bornée `for`](https://glassus.github.io/premiere_nsi/T1_Demarrer_en_Python/1.2_Boucle_for/cours/) ou encore [ma bafouille à propose de la notion d'itérer en Python](https://sebhoa.gitlab.io/s1sc122/02_Python/iterer/)
    2. [l'instruction conditionnelle `if`](https://sebhoa.gitlab.io/s1sc122/02_Python/ifelse/) 

Voici comment parcourir les _events_ :

```python
for event in pygame.event.get():
    # ici nous allons traiter l'événement event
```

??? summary "Les différents type d'_event_"

    Lorsqu'on a un _event_, on a accès à son type : `event.type`. Pygame définit un certain nombre de type d'_event_ dont voici les principaux :

    | type d'événement         | provoqué par...                      |
    | ------------------------ | ------------------------------------ |
    | `pygame.QUIT`            | fermeture de la fenêtre              |
    | `pygame.KEYDOWN`         | enfoncement d'une touche du clavier  |
    | `pygame.KEYUP`           | relâchement d'une touche du clavier  |
    | `pygame.MOUSEMOTION`     | mouvement de la souris               |
    | `pygame.MOUSEBUTTONUP`   | enfoncement d'un bouton de la souris |
    | `pygame.MOUSEBUTTONDOWN` | relâchement d'un bouton de la souris |

Nous pouvons modifier notre classe `Jeu` pour quitter lorsque la _croix de la fenêtre_ est utilisée :

```python
class Jeu:
    ...
    def loop(self):
        fini = False
        while not fini:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    fini = True
```
   
Mais, vous vous rendez-compte que notre variable booléenne `fini` ne sert pas à grand-chose puisque dès qu'on clique sur la croix de toute façon la fenêtre se ferme, quittant tout. 

!!! success "`snake_05.py`"

    ```python
    --8<-- "05_from_scratch/scripts/snake_05.py"
    ```

## Exercice

!!! question "À faire vous-même"

    === "Énoncé"

        Dans un fichier `snake_06.py` changer votre précédent script pour :

        1. Afficher un coucou (comme au début) lorsque l'utilisateur clique ; 
        2. Quitter en disant `Au revoir`, lorsqu'on appuie sur une touche du clavier

    === "Solution"

        ```python
        --8<-- "05_from_scratch/scripts/snake_06.py"
        ```

Nous constatons que les événements sont très pratiques et nous pouvons maintenant les manipuler. Lorsque l'on a un événement d'un certain type, cet événement possède des propriétés liées à ce type. Par exemple, si `event` est de type `pygame.KEYDOWN` alors la propriété `key` permet de savoir à quelle touche on a à faire. 

Il existe une constante pygame pour chaque touche. On trouve ce tableau ici : [les constantes pygame pour les touches](https://devdocs.io/pygame/ref/key#key-constants-label).

Par exemple si le test `event.key == pygame.K_UP` est vrai cela signifie qu'on a pressé la touche flèche vers le haut de notre clavier.

Tout est pratiquement en place pour commencer à modéliser notre jeu SNAKE. Rendez-vous au chapitre suivant.


[^1]: [Écouter Wendy Mackay](https://www.college-de-france.fr/site/wendy-mackay/inaugural-lecture-2022-02-24-18h00.htm), spécialiste des interfaces Humain-Machine, lors de la leçon inaugurale de la chaire d'Informatique 2021--2022, au Collège de France.