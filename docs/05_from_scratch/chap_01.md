**CHAPITRE 1**

--- 

# Un premier objet

En Python, on définit un objet en utilisant le mot clé `class` et en lui donnant un nom. Notre premier objet s'appelle `Jeu` :

```python
class Jeu:
    pass
```

L'instruction `pass` sert juste à dire à Python qu'il n'y a rien à faire. Il nous faut un programme principal, ie un ensemble d'**instructions** que l'interprète Python va exécuter. Ici on veut simplement créer un exemplaire de notre jeu. En programmation on appelle **instance** un exemplaire d'un objet.

Pour créer cette instance, nous faisons ceci :

```python
Jeu()
```

Les parenthèses sont là pour dire que nous allons construire une instance de l'objet `Jeu`. D'autre part, nous allons donner un nom à cette instance, pour, plus tard, pouvoir la nommer et éventuellement lui demander des choses.

En langage Python, pour nommer un objet, on écrit ce nom, qu'on appelle un **identificateur** puis on écrit le symbole **=** et enfin l'objet qu'on est en train de nommer. Dans notre cas, l'objet est une instance de `Jeu` réalisée par `Jeu()`, et pour le nom appelons le `snake`. Ce qui donne :

```python3
snake = Jeu()
```

`Jeu()` joue le rôle de **constructeur**, il nous permet de construire des instances de l'objet `Jeu`.

!!! tip "Astuce"

    Notez bien la différence, sans parenthèses : l'objet ; avec parenthèses l'appel au constructeur.


## Premier programme

!!! question "À faire vous-même"

    Avec votre environnement de programmation, créez un nouveau fichier, que vous pouvez nommer `snake_00.py` (l'extension .py signifie qu'il s'agit d'un programme Python) et saisissez-y le code suivant :

    ```python
    class Jeu:
        pass

    snake = Jeu()
    ```

    Exécutez-le. Il ne devrait rien se passer :smile:

!!! question "À faire vous-même"

    === "Énoncé"
    
        Reprenez votre programme `snake_00.py` et effectuez les changements suivants :
        
        1. Supprimez les `:`, sauvegardez et exécutez le programme...
        2. Supprimez l'indentation, sauvegardez et exécutez... 

    === "Solution"

        1. vous avez du avoir un message d'erreur du genre :
          ```
          File "/un/chemin/vers/votre/snake_00.py", line 1
          class Jeu
                   ^
          SyntaxError: invalid syntax
          ```
        2. vous avez du obtenir une autre erreur :
            ```
            File "/un/chemin/vers/votre/snake_00.py", line 2
            pass
            ^
            IndentationError: expected an indented block
            ```

??? info "Les concepts"

    1. [Règles de syntaxes, bonnes pratiques](https://www.carnets.info/python/pep8/)
    2. Notion de programme et d'instruction
    3. [Objet, valeur et type](https://sebhoa.gitlab.io/s1sc122/02_Python/variables/)
    4. [Identifiant, variable, référence](https://sebhoa.gitlab.io/s1sc122/02_Python/variables/)
    5. [Affectation](https://sebhoa.gitlab.io/s1sc122/02_Python/variables/) 


## Améliorer le programme

Ce premier programme ne faisait rien du tout. Nous allons l'améliorer un peu pour qu'il affiche `Coucou` à l'écran.

!!! question "À faire vous-même"

    Avec votre environnement de programmation, créez un nouveau fichier, que vous pouvez nommer `snake01.py` saisissez-y le code suivant :

    ```python
    class Jeu:

        def annonce(self):
            print('Coucou')

    snake = Jeu()
    snake.annonce()
    ```

    Exécutez-le.

Notre nouveau programme manipule beaucoup plus de choses que le premier :

- une **méthode** : `annonce` c'est une _action_ que peut effectuer notre objet `Jeu`. `snake.annonce()` est l'instruction qui permet à notre `Jeu` `snake` d'exécuter son action, on dit qu'il **invoque** la méthode `annonce`. Cette méthode effectue quant à elle un **appel** à ... 
- une fonction prédéfinie `print` : il s'agit d'une fonction connue par l'interprète Python (un peu comme votre calculatrice connait la fonction cosinus) et qui effectue l'affichage à l'écran de ce qu'on lui donne
- une chaine de caractères `'Coucou'` qui est donc ce qu'on donne à la fonction `print` à afficher ; on dit que `'Coucou'` est un **argument**

??? info "Les concepts"

    1. Fonction prédéfinie
    2. Chaine de caractères
    3. Appel de fonction, passage d'argument
    

## Nouveaux objets

Les chaines de caractères (comme `'Coucou'`) sont connues par Python sous le nom de `str`. On parle de type. Ainsi `'Coucou'` est de type `str`.

Est-ce que `snake` est aussi une `str` ? Pas du tout. 

!!! warning "Ne pas confondre identificateur et `str`"

    Comment les différencier ?

    La chaine de caractères commence **toujours** par un délimiteur c'est-à-dire un ou plusieurs symboles qui vont marquer le début de la chaine et qu'on retrouvera à la fin, pour marquer la fin de la chaine. Les délimiteurs possibles sont :

    - l'apostrophe ou _quote_ en anglais : `'Hello'`, 
    - les guillemets ou _double quote_ : `"Ceci est une autre chaine un peu plus longue"`
    - les triple (double) _quote_ :
       ```python
       """Ceci est une longue
       longue chaine
       sur plusieurs lignes !"""
       ```
    Parfois on doit faire attention au choix du délimiteur ; dans l'exemple ci-dessous, notre chaine contient une apostrophe : nous ne pouvons donc pas nous en servir comme délimiteur puisque Python ne saurait plus où se situe la fin de la chaine. 

    ```python
    'Lundi c'est ravioli'
    ```

    Il faut utiliser les guillements ici :

    ```python
    "Lundi c'est ravioli"
    ```

    Si vous avez un bon éditeur de texte alors la colorisation syntaxique (le fait que certains mots sont de couleur différente) devrait vous ader à détecter ce type d'erreur.

!!! warning "Abus de langage"

    Parfois, suite à une **instruction** d'**affectation** :

    ```python
    ma_chaine = 'Je programme en Python'
    ```

    Vous pourriez entendre dire que `ma_chaine` est de type `str` ou est _un `str`_... il s'agit d'un raccourci de langage pour dire que l'objet **référencé** par la variable `ma_chaine` est de type `str`. 

    **En Python, les variables ne sont pas typées, seul les objets le sont.**

## Une troisième version

Le rôle des méthodes et fonctions, c'est de rendre le programme modulaire et _personnalisable_. Par exemple ici, ce serait sympa, au moment de la création du `Jeu` de choisir si la méthode `annonce` va afficher `Coucou` ou bien `Hello` ou encore `Bonjour`... Nous allons rajouter une **propriété** à notre objet et c'est la valeur de cette propriété qui sera utilisée.

Il nous faut un identifiant, par exemple `message`. Pour ajouter une propriété, il nous faut définir une méthode particulière qui s'appelle **initialiseur** ; voici comment :

```python
class Jeu:
    
    def __init__(self, msg):
        self.message = msg
```

Sans trop vouloir vous ennuyer avec la syntaxe pour le moment, mais comme nous allons les rencontrer tout au long de cette initiation, précisons un peu le rôle de quelques éléments :

- nos objets Python auront des propriétés, comme les objets du quotidien
- ils ont aussi des méthodes qu'ils peuvent invoquer pour réaliser des actions, ces méthodes sont techniquement des fonctions
- `__init__` est une méthode particulière : c'est la première rencontrée et elle permet d'initialiser les propriétés de notre objet
- `self` représente l'objet lui-même ; ce petit identificateur doit être le premier paramètre de **toutes les méthodes**.

??? info "Les concepts"

    1. Définition de fonction avec paramètres
    2. Précisions sur le vocabulaire de la POO


Notre méthode `annonce` doit subir un petit changement aussi pour tenir compte que maintenant il ne faut pas afficher systématiquement la chaine de caractères `Coucou`, mais la chaine de caractères qui sera référencée par `self.message` :

```python
class Jeu:
    ...
    def annonce(self):
        print(self.message)
```

Dès lors, le programme principal devient :

```python
snake = Jeu('Salut')
snake.annonce()
```

Et on pourrait même envisager la création de 2 instances, un peu différentes 

!!! question "À faire vous-même"


    === "Énoncé"
    
        Dans un fichier `snake_02.py` définir une classe `Jeu` comme vu précédemment puis un programme principal qui crée deux instances différentes et qui invoque chacunes leur méthode `annonce`.

    === "Solution"
        ```python
        --8<--  "05_from_scratch/scripts/snake_02.py"
        ```
