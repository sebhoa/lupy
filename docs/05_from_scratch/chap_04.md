**CHAPITRE 4**

--- 

# Modéliser

Nous avons parlé du module qui va permettre la visualisation de notre jeu. Il nous faut maintenant créer le modèle, c'est-à-dire les différents objets pour les entités du jeu.

Bien que l'envie soit forte de parler tout de suite du serpent, le premier objet à modéliser est l'Arène dans laquelle le serpent va évoluer. C'est cette Arène qui aura en charge la gestion des pommes, nourriture du serpent.

!!! tip "Astuce"

    En programmation orientée objet, il n'est pas rare de créer beaucoup de petits objets qui vont interagir. Chacun aura un rôle bien précis et sera capable de faire de petites actions, rendant l'ensemble lisible et simple.

## L'objet Arène

Créé par notre objet `Jeu`, cet objet va créer le serpent et les pommes. Il va récupérer et mémoriser l'écran du jeu déjà créé. Dans un premier temps, nous n'allons pas créer de pommes, uniquement un serpent.

```python
class Arene:
    
    def __init__(self, jeu):
        self.jeu = jeu
        self.ecran = jeu.ecran
        self.serpent = Serpent(self)
```

La présence de `self` dans la création du serpent, permet de dire au serpent : 

> je suis ~~ton père~~ !... ton Arène. 
    

Notre classe `Jeu`:

```python
class Jeu:
    
    def __init__(self):
        self.ecran = pygame.display.set_mode((LARGEUR, HAUTEUR))
        self.arene = Arene(self)
```

## L'objet Serpent

Pour le modéliser il va falloir _discrétiser_ notre écran, c'est-à-dire considérer la surface de l'Arène comme une grille de petits carrés de $N\times N$. Voici un schéma d'un serpent de longueur 4 :

![serpent](images/move01.png){ .centrer }

Ainsi nous constatons que le serpent est représenté par un tableau de 4 coordonnées (des couples d'entiers), ici le tableau suivant :

```python
[(28, 22), (29, 22), (30, 22), (31, 22)]
```

??? info "Les concepts"

    1. les n-uplets (`tuple`) 
    2. [les tableaux (`list`)](https://glassus.github.io/premiere_nsi/T2_Representation_des_donnees/2.1_Listes/cours/) y compris la construction en compréhension
    3. l'objet `range`

En plus du tableau des coordonnées, il nous faut savoir où se trouve la tête (dans le schéma précédent elle se trouve en dernière position du tableau). À la création du serpent, nous le positionnons au milieu de l'Arène. D'autre part, nous allons donner à notre serpent un accès à son arène et lui dire sur quel écran il va évoluer (se dessiner) :

```python
class Serpent:
    
    def __init__(self, arene):
        self.arene = arene
        self.ecran = arene.ecran
        self.pos = [(COLS//2 + i, ROWS//2) for i in range(-LENGTH//2, LENGTH//2)]
        self.tete = -1
```

## De nouvelles constantes

**Que sont `COLS` et `ROWS` ?** Il s'agit du nombre de colonnes et du nombre de lignes de notre grille _virtuelle_ plaquée sur notre écran. Comment calcule-t-on ces grandeurs ?

La largeur de notre écran est référencée par le constante `LARGEUR`. Nos carrés de grille font une certaine taille (pixels) disons 10. Ce 10 nous allons aussi le mémoriser comme une constante :

```python
SIZE = 10
```

Dès lors en divisant la largeur par cette taille nous obtenons le nombre de colonnes (et de façon duale avec la hauteur pour le nombre de lignes) :

```python
COLS = LARGEUR // SIZE
ROWS = HAUTEUR // SIZE
```

**Pourquoi -1 pour la position de la tête ?** Parce que nous avons décidé de mettre la tête en fin de tableau. Un indice -1 représente la dernière valeur d'un tableau.

## Script intermédiaire

Si vous avez intégré les dernières avancées au script `snake_06.py` vous devriez avoir quelque chose comme :

```python title="snake_06b.py -- non fonctionnel"
import pygame
from constantes import *

class Serpent:
    def __init__(self, arene):
        self.arene = arene
        self.ecran = arene.ecran
        self.pos = [(COLS//2 + i, ROWS//2) for i in range(-LENGTH//2, LENGTH//2)]
        self.tete = -1


class Arene:
    def __init__(self, jeu):
        self.jeu = jeu
        self.ecran = jeu.ecran
        self.serpent = Serpent(self)


class Jeu:
    def __init__(self):
        self.ecran = pygame.display.set_mode((LARGEUR, HAUTEUR)) 
        self.arene = Arene(self)   

    def start(self):
        pygame.init()
        self.loop()

    def loop(self):
        fini = False
        while not fini:
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    fini = True
        pygame.quit()    
    
snake_game = Jeu()
snake_game.start()
```

Mais vite, passons au chapitre suivant pour afficher notre serpent.