**CHAPITRE 0**

--- 

# Avant de commencer...

Pour suivre ce _cours_ il vous faut :

- un ordinateur avec un système d'exploitation relativement récent et à jour : windows, linux ou mac osx, les trois seront opérationnels
- nous allons écrire et exécuter des programmes écrits en [Python3](https://www.python.org/) : il vous faut un interprète et un environnement de programmation.

## Installer un interprète Python

=== "Windows"

    1. [Télécharger Python](https://www.python.org/downloads/release/python-3102/) depuis le site officiel
    2. Installer en veillant à bien cocher la petite case relative à (la variable d'environnement) PATH :
    ![install](images/install_python_windows.png)
    3. Vérifier que tout est ok une fois l'installation faite ; trouver l'application "Invite de commande" et lancez là. Dans la fenêtre tapez la commande :

    ```
    python
    ```

    ou 

    ```
    python3
    ```

    Vous devriez obtenir une réponse similaire à ceci :

    ```
    Python 3.9.2 (tags/v3.9.2:1a79785, Feb 19 2021, 13:44:55) [MSV v.1928 64 bit (AMD 64)] on win32
    Type "help", "copyright", "credits" or "licence" for more information
    >>> _
    ```

    

    En cas de souci, il existe de nombreuses vidéos sur le net pour vous aider à installer Python sur votre ordinateur windows. En voici une [installer proprement Python sous Windows](https://www.youtube.com/watch?v=3nrCgMTDTdY)


=== "Mac OSX ou Linux"

    Vous n'avez rien à faire a priori puisque ces deux systèmes sont _livrés_ avec un interprète installé. Vous pouvez vérifier en lançant l'application Terminal puis en tapant dans cette application la commande suivante :

    ```bash
    python3
    ```

    Vous devriez obtenir une réponse similaire à ceci :

    ```
    Python 3.9.7 (default, Sep 10 2021, 14:59:43) 
    [GCC 11.2.0] on linux
    Type "help", "copyright", "credits" or "license" for more information.
    >>>
    ``` 

## Installer un environnement de programmation

L'environnement de programmation va vous permettre à la fois de saisir le texte de vos programmes (on parle de _code source_) et de les exécuter.

L'environnement _à la mode_ actuellement est [Visual Studio Code](https://code.visualstudio.com/#alt-downloads) ou son équivalent libre [VSCodium](https://vscodium.com/).

_rajouter une aide vidéo sur lancer son premier programme_