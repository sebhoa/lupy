**CHAPITRE 2**

--- 

# Créer une fenêtre graphique

## Choisir et installer sa bibliothèque graphique

Notre objet `Jeu` n'a pas trop d'intérêt pour l'instant. Afficher un message à l'écran, même si ça peut s'avérer utile, ça ne va pas constituer la finalité d'un jeu passionnant. 

Ce que nous souhaitons, c'est réaliser une application graphique. Nous voulons donc une méthode `start` qui, une fois lancée nous crée une fenêtre graphique.

Il nous faut une interface graphique. En général, ces interfaces mettent en place tout un ensemble d'objets, de méthodes qui ne font pas partie de l'interprète Python de base. Nous devons **importer** ce qu'on appelle un **module** ; on parle aussi de **bibliothèque** (attention à ne pas dire _librairie_ qui vient d'une mauvaise traduction du terme anglais pour bibliothèque : _library_).

L'interface que nous allons utiliser dans le cadre de cette initiation est _pygame_. Le site officiel [pygame.org](https://www.pygame.org/) renseigne sur la procédure à suivre pour l'installation du module. Si vous avez installé un interprète récent, vous devriez pouvoir installer très simplement via l'outil [pip](https://pypi.org/project/pip/). La commande à taper devrait être (éventuellement en remplaçant `pip3` par `pip`) :

```bash
pip3 install pygame
```

Ensuite, nous devons commencer notre programme par :

```python
import pygame
```

!!! tip "Documentation"
    
    Le site offre une documentation, on en trouve une autre, mieux faite ici : [devdocs.io/pygame/](https://devdocs.io/pygame/). Pour une documentation en français (mais il est bon de pratiquer l'anglais si on souhaite persévérer en Informatique et en programmation en particulier) il existe une [version numérique payante aux éditions ENI](https://www.editions-eni.fr/livre/pygame-initiez-vous-au-developpement-de-jeux-video-en-python-9782409021688)

Un des objets Pygame est la `Surface`, nous l'étudirons plus en détail. Pour l'instant, nous allons créer une surface particulière, une sorte de fond qui servira de support à notre `Jeu`. Cette surface est créée par :

```python
pygame.display.set_mode((une_largeur, une_hauteur))
```

## Travailler avec des constantes

Les constantes sont des variables comme les autres, si ce n'est que les valeurs associées ne seront **jamais** modifiées. Afin de structurer son programme il peut être utile de regrouper l'ensemble de ses constantes dans un fichier à part, nommé par exemple `constantes.py`. 

!!! question "À faire vous-même"

    Créer un fichier `constantes.py` et mettez-y vos deux premières constantes :

    ```python
    LARGEUR = 600
    HAUTEUR = 450
    ```

Si vous avez lu des cours parlant nommage et convention, vous savez que les identifiants des constantes sont en majuscules. Rappelons aussi :

- en `MAJUSCULES`, les constantes
- en `snake_case` le nom des fonctions et des méthodes : `def ma_fonction_utile(...)` (tout en minuscule, les différents mots séparés par des soulignés, _underscore_ en anglais)
- en `CamelCase` pour le nom des classes : `class MonSuperJeu:` (chaque nouveau mot qui constitue le nom commence par une majuscule)

Nous utiliserons alors l'instruction suivante dans notre programme pour avoir accès à l'ensemble des constantes :

```python
from constantes import *
```

## Une première fenêtre

Nous allons complètement revoir notre objet `Jeu`. Il va devenir un _vrai_ jeu.

Dans un premier temps, cet objet va embarquer une propriété `ecran` pour référencer notre fenêtre graphique et nous initialiserons l'ensemble des modules de la bibliothèque pygame (oui pygame est plus qu'un module) dans une méthode `start`. 

!!! question "À faire vous-même"

    === "Énoncé"
        
        Dans un fichier `snake_03.py`, 
        
        1. Importer le module `pygame` ainsi que votre fichier de constantes
        2. Définir une classe `Jeu` qui possède :

            - un initialiseur pour définir une proprité `ecran` référençant une surface _display_ de pygame
            - une méthode `start` qui initiatilise tous les sous modules de pygame par un `pygame.init()`

        3. Ajouter un programme principal qui crée une instance du jeu et lance la méthode `start`

    === "Solution"
        ```python
        --8<-- "05_from_scratch/scripts/snake_03.py"
        ```

Lorsque vous exécutez ce programme, tout est ok, si :

1. vous avez brièvement vu s'ouvrir et se refermer une fenêtre toute noire
2. ce message (aux valeurs de version près) est affiché sur votre console ou terminal :
   > pygame 2.0.1 (SDL 2.0.14, Python 3.9.2)<br>
   > Hello from the pygame community. https://www.pygame.org/contribute.html

## Garder cette fenêtre ouverte

Il serait intéressant de pouvoir garder cette fenêtre ouverte plus de 1s. Ce qui se passe pour le moment, c'est que le programme se lance, crée notre instance de `Jeu`, lance la méthode _start_  de notre jeu ce qui crée la fenêtre graphique. Puis, comme on n'y fait rien de spécial, le programme se termine et détruit la fenêtre.

Pour garder la fenêtre ouverte, il faut empêcher le programme de se terminer si vite. Nous allons ajouter une méthode à notre `Jeu`. Son rôle : créer une boucle qui ne s'arrête que... si la partie est terminée. Tant que cette boucle s'exécute, la fenêtre reste ouverte (puisque le programme n'est pas fini). Quand on le décide, on met fin au programme et le module pygame détruit tous ses objets par l'instruction : `pygame.quit()`.

Pour cela nous rajoutons une méthode `loop` à notre classe `Jeu`. Cette méthode va demander à l'utilisateur d'entrer une valeur (**attention** l'interaction va se dérouler dans une console texte qui n'a rien à voir avec la fenêtre graphique) :

```python linenums="1"
class Jeu:
    ...
    def loop(self):
        fini = False
        while not fini:
            reponse = input('Tapez ce que vous voulez, puis entrée pour terminer')
            fini = True
        pygame.quit()
```

!!! question "Mini quiz"

    === "Énoncé"

        À la ligne 4 que code ci-dessus, nous avons à faire à :

        - [ ] Une équation 
        - [ ] Une instruction
        - [ ] Une expression
        - [ ] Une affectation

    === "Réponses"


        - [ ] Une équation (Ça c'est en maths :smile:)    
        - [x] Une instruction 
        - [ ] Une expression (Non, une expression calcule une valeur : `2 + 3` ou `x > 0`)
        - [x] Une affectation 
        

??? info "Les concepts"

    1. [L'objet booléen](https://glassus.github.io/premiere_nsi/T2_Representation_des_donnees/2.5_Booleens/cours/)
    2. [La notion de boucle non bornée `while`](https://glassus.github.io/premiere_nsi/T1_Demarrer_en_Python/1.3_Boucle_while/cours/) et aussi autre page qui pointe les [difficultés didactiques de la boucle `while`](https://sebhoa.gitlab.io/s1sc122/02_Python/iterer/#difficulte-didactique-du-while) 
    3. Les entrées / sorties, interactions (_pages à faire ou trouver_)

!!! question "À faire vous-même"

    === "Énoncé"

        Regroupez tout ce qu'on vient de voir dans un fichier `snake_04.py`. Sauvez et exécutez.

    === "Solution"
        ```python linenums="1"
        --8<-- "05_from_scratch/scripts/snake_04.py"
        ```

Et nous avons notre première fenêtre graphique stable :

![ecran noir](images/ecran_noir.png){ .centrer }


En général, dans une vraie application graphique, le programme est lancé et ensuite est à l'écoute des interactions de l'utilisateur : utilisation du clavier, clic ou mouvement de la souris, fermeture de la fenêtre en cliquant sur la petite croix rouge....

Toutes ces actions sont des **événements** (_event_ en anglais) et le module pygame offre la possibilité de les gérer. C'est ce que nous allons voir dans le chapitre suivant.